#!/usr/bin/env python3


import sys,subprocess,argparse
from io import StringIO

parser = argparse.ArgumentParser()
parser.add_argument("fasta", help="fasta sequences")
parser.add_argument("p", help="proceed p percent of the file (from 0 to p percent)")
args = parser.parse_args()

p = args.p
fastaf = open(args.fasta)
tempFasta = open("partial/part_" + p + ".fasta","w")

allList = fastaf.readlines()
totalSeq = len(allList)

firstLine = round(totalSeq*(int(p)-1)/100.0)
lastLine = round(totalSeq*int(p)/100.0)

cursor = firstLine
inSeq = False
lastSeq = False
while cursor < totalSeq:
    line = allList[cursor]
    if not inSeq:
        if line.startswith(">"):
            inSeq = True
    if lastSeq:
        if line.startswith(">"):
            break
    if cursor == lastLine:
        lastSeq = True
    if inSeq:
        tempFasta.write(line)
    cursor += 1
    
