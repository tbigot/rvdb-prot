#!/usr/bin/env python3

import sys,subprocess,argparse
from io import StringIO

parser = argparse.ArgumentParser()
parser.add_argument("master", help="fasta containing all sequences")
parser.add_argument("silix", help="result of Silix computation")
parser.add_argument("destination", help="directory in which create new fasta files (must exist, should be empty)")
parser.add_argument("--minSize", help="minimum number of sequences to be considered as a cluster",type=int,default=2)
args = parser.parse_args()


# load clusters in memory
clusters = map(lambda l: l.strip().split(),open(args.silix))
seqToFam = {l[1]: l[0] for l in clusters}


from collections import Counter
famSize = Counter(seqToFam.values())


def addSeqToFamilyFile(name,sequence):
    if len(name) == 0:
        return
    elif famSize[seqToFam[name.split()[0]]] < args.minSize:
        return
    family = seqToFam[name.split()[0]]
    ofasta = open(args.destination + "/" + family + ".fasta","a")
    ofasta.write(">" + name + "\n" + "".join(sequence))


# now sorting the sequences of fasta
currName = ""
currSequence = []
sequences = open(args.master)
for line in sequences:
    if line.startswith(">"):
        addSeqToFamilyFile(currName,currSequence)
        currName = line.strip()[1:]
        currSequence = []
    else:
        currSequence.append(line)
addSeqToFamilyFile(currName,currSequence)
