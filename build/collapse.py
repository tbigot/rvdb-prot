#!/usr/bin/env python3
import argparse,fastaHmmr

parser = argparse.ArgumentParser()
parser.add_argument("fastaFile", help="original set of sequences")
parser.add_argument("--fractionID", help="Fraction identity",default=1.0)
parser.add_argument("--fractionCov", default=None)
parser.add_argument("--cores", default=1)
args = parser.parse_args()

fastaHmmr.collapseSequence(args.fastaFile,args.fractionID,args.fractionCov,args.cores)
