#!/usr/bin/env python3
import argparse,fastaHmmr

parser = argparse.ArgumentParser()
parser.add_argument("fastaFile", help="original set of sequences")
parser.add_argument("minSequenceLength", help="minimum sequence length")
args = parser.parse_args()

fastaHmmr.filterFASTA(args.fastaFile,int(args.minSequenceLength))
