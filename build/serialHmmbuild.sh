#! /bin/sh
#SBATCH --qos=fast 
#SBATCH --cores=1
date >> log.txt
echo "HMMBUILD ${SLURM_ARRAY_JOB_ID}.${SLURM_ARRAY_TASK_ID}.">> log.txt
srun python3 /pasteur/homes/tbigot/bank/scripts/partialHmmbuild.py /pasteur/homes/tbigot/bank/rVDB_prot/aligned ${SLURM_ARRAY_TASK_ID}
exit 0
