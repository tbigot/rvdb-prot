#! /bin/sh
#SBATCH --cores=4
date >> log.txt
echo "Job ${SLURM_ARRAY_JOB_ID}.${SLURM_ARRAY_TASK_ID}.">> log.txt
srun python3 /pasteur/homes/tbigot/bank/scripts/partialBlast.py /pasteur/homes/tbigot/bank/rVDB_prot/rVDB_prot2_plus_noDupes_minL1_s100.fasta ${SLURM_ARRAY_TASK_ID}
srun blastp -query partial/part_${SLURM_ARRAY_TASK_ID}.fasta -out result/part_${SLURM_ARRAY_TASK_ID}.out -db /pasteur/homes/tbigot/bank/rVDB_prot/rVDB_prot2_plus_noDupes_minL1_s100.fasta -outfmt 6 -num_threads 4
exit 0
