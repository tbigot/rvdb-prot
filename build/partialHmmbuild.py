#!/usr/bin/env python3


import sys,subprocess,argparse,glob,os

parser = argparse.ArgumentParser()
parser.add_argument("dir", help="directory containing alignments")
parser.add_argument("p", help="proceed p percent of the file (from 0 to p percent)")
args = parser.parse_args()

p = args.p
fastas = glob.glob(args.dir +"/*.msa.faa")
log = open(p + ".log","w")
nbFiles = len(fastas)

firstFile = int(round(nbFiles*(int(p)-1)/100.0))
lastFile = int(round(nbFiles*int(p)/100.0))

for cfp in fastas[firstFile:lastFile]:
    familyName = '.'.join(os.path.basename(cfp).split(".")[:-2])
    outp = familyName + ".hmm"
    logp = familyName + ".log"
    log.write((subprocess.check_output("hmmbuild --informat afa -n %s -o %s %s %s" % (outp,logp,outp,cfp),shell=True)).decode("utf-8"))
