#!/usr/bin/env python3


import sys,subprocess,argparse,glob,os

parser = argparse.ArgumentParser()
parser.add_argument("dir", help="directory containing fasta")
parser.add_argument("p", help="proceed p percent of the file (from 0 to p percent)")
args = parser.parse_args()

p = args.p
fastas = glob.glob(args.dir +"/*.fasta")
log = open(p + ".log","w")
nbFiles = len(fastas)

firstFile = int(round(nbFiles*(int(p)-1)/100.0))
lastFile = int(round(nbFiles*int(p)/100.0))

for cfp in fastas[firstFile:lastFile]:
    outp = os.path.splitext(os.path.basename(cfp))[0] + ".msa.faa"
    logp = os.path.splitext(os.path.basename(cfp))[0] + ".log"
    log.write((subprocess.check_output("muscle -in %s -out %s -log %s" % (cfp,outp,logp),shell=True)).decode("utf-8"))
