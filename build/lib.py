#!/usr/bin/env python3

from typing import NamedTuple
import itertools

### Fasta

def fasta(path,string=False):
    currName = None
    currSeq = []
    for line in open(path):
        if line.startswith(">"):
            if currName:
                yield (currName,("".join(currSeq) if string else currSeq))
                currSeq = []
            currName = line[1:-1]
        else:
            currSeq.append(line[:-1])
    if currName:
        yield (currName,("".join(currSeq) if string else currSeq))

def fastq(path,string=False):
    currQ = []
    for line in open(path):
        currQ.append(line)
        if len(currQ) == 4:
            yield currQ
            currQ = []



### Sequences
compl_transtable = str.maketrans("aAcCgGtT","tTgGcCaA")
codontable = {
      'SBM' : 'X', 'TCS' : 'S', 'MGC' : 'X', 'YMV' : 'X', 'TRG' : 'X', 'HMW' : 'X', 'HVT' : 'X', 'CVY' : 'X', 'DDM' : 'X', 'GGV' : 'G', 
'ABC' : 'X', 'MNB' : 'X', 'BYM' : 'X', 'NAV' : 'X', 'SGR' : 'X', 'BGM' : 'X', 'NNS' : 'X', 'DGA' : 'X', 'RWA' : 'X', 'AAV' : 'X', 
'CVK' : 'X', 'SHG' : 'X', 'SAY' : 'X', 'GBV' : 'X', 'GTM' : 'V', 'TND' : 'X', 'CWC' : 'X', 'AKS' : 'X', 'TMN' : 'X', 'NYV' : 'X', 
'VDA' : 'X', 'VDM' : 'X', 'VYH' : 'X', 'GWG' : 'X', 'HCK' : 'X', 'BAD' : 'X', 'WRB' : 'X', 'HKS' : 'X', 'MCK' : 'X', 'TSM' : 'X', 
'VYG' : 'X', 'KRS' : 'X', 'TVH' : 'X', 'MVD' : 'X', 'CHT' : 'X', 'TDT' : 'X', 'HWK' : 'X', 'CHS' : 'X', 'TTB' : 'X', 'WHA' : 'X', 
'DTH' : 'X', 'TGG' : 'W', 'WWN' : 'X', 'AWW' : 'X', 'CCW' : 'P', 'YBN' : 'X', 'CYG' : 'X', 'DVB' : 'X', 'MRS' : 'X', 'MKK' : 'X', 
'WMC' : 'X', 'SCS' : 'X', 'YRB' : 'X', 'HGR' : 'X', 'GDB' : 'X', 'KVS' : 'X', 'MWA' : 'X', 'ANW' : 'X', 'RTC' : 'X', 'VCM' : 'X', 
'VRK' : 'X', 'AVY' : 'X', 'WTH' : 'X', 'VKC' : 'X', 'ABW' : 'X', 'MBY' : 'X', 'GVC' : 'X', 'BTA' : 'X', 'VDW' : 'X', 'TRM' : 'X', 
'WBD' : 'X', 'DKY' : 'X', 'TVG' : 'X', 'HKN' : 'X', 'ACS' : 'T', 'NTC' : 'X', 'AHW' : 'X', 'HVK' : 'X', 'GMT' : 'X', 'VYV' : 'X', 
'RNC' : 'X', 'WDG' : 'X', 'BHY' : 'X', 'HYV' : 'X', 'KND' : 'X', 'ATR' : 'X', 'NRG' : 'X', 'WKK' : 'X', 'TNG' : 'X', 'SGW' : 'X', 
'RTW' : 'X', 'TWB' : 'X', 'AMD' : 'X', 'BWW' : 'X', 'TVY' : 'X', 'THK' : 'X', 'MTD' : 'X', 'WKA' : 'X', 'HYD' : 'X', 'BSM' : 'X', 
'GAT' : 'D', 'NYY' : 'X', 'SNV' : 'X', 'SAV' : 'X', 'SGA' : 'X', 'RKC' : 'X', 'YCN' : 'X', 'RCG' : 'X', 'GCD' : 'A', 'TKT' : 'X', 
'NVH' : 'X', 'MHC' : 'X', 'AWN' : 'X', 'MVH' : 'X', 'SWR' : 'X', 'BSK' : 'X', 'WAH' : 'X', 'HMM' : 'X', 'SGG' : 'X', 'MMA' : 'X', 
'KHA' : 'X', 'CTT' : 'L', 'TSH' : 'X', 'AKW' : 'X', 'SWC' : 'X', 'TYG' : 'X', 'RCM' : 'X', 'NKM' : 'X', 'MDA' : 'X', 'WDM' : 'X', 
'HRC' : 'X', 'GHR' : 'X', 'BDG' : 'X', 'GCR' : 'A', 'MTT' : 'X', 'GBK' : 'X', 'GCK' : 'A', 'VVK' : 'X', 'CWK' : 'X', 'CRT' : 'X', 
'CTB' : 'L', 'NTD' : 'X', 'DDT' : 'X', 'GWS' : 'X', 'VAR' : 'X', 'TRY' : 'X', 'YBY' : 'X', 'CKK' : 'X', 'VTD' : 'X', 'KBA' : 'X', 
'BMC' : 'X', 'SCR' : 'X', 'RTS' : 'X', 'KHS' : 'X', 'MGM' : 'X', 'KAM' : 'X', 'TDD' : 'X', 'YKM' : 'X', 'RBW' : 'X', 'TSD' : 'X', 
'VMS' : 'X', 'YAG' : 'X', 'SNT' : 'X', 'BKK' : 'X', 'SCW' : 'X', 'TYR' : 'X', 'CAC' : 'H', 'RAY' : 'X', 'TNH' : 'X', 'VAY' : 'X', 
'TAR' : None, 'GGG' : 'G', 'GHM' : 'X', 'YSK' : 'X', 'MYH' : 'X', 'WRV' : 'X', 'WYW' : 'X', 'NGR' : 'X', 'KTK' : 'X', 'GAA' : 'E', 
'NWC' : 'X', 'BHR' : 'X', 'ABV' : 'X', 'YGW' : 'X', 'TGR' : 'X', 'NBR' : 'X', 'DAD' : 'X', 'BVM' : 'X', 'HTS' : 'X', 'ABS' : 'X', 
'YWC' : 'X', 'KGW' : 'X', 'TAN' : 'X', 'YMB' : 'X', 'SNY' : 'X', 'SDK' : 'X', 'HWN' : 'X', 'GKC' : 'X', 'HHY' : 'X', 'HTV' : 'X', 
'MHD' : 'X', 'YBB' : 'X', 'GCY' : 'A', 'GTR' : 'V', 'BAA' : 'X', 'AKM' : 'X', 'TCR' : 'S', 'ANM' : 'X', 'SKH' : 'X', 'BWN' : 'X', 
'SBG' : 'X', 'MWB' : 'X', 'CYC' : 'X', 'BHN' : 'X', 'DWD' : 'X', 'CBY' : 'X', 'SYM' : 'X', 'MTB' : 'X', 'WNC' : 'X', 'NDR' : 'X', 
'HVN' : 'X', 'WHG' : 'X', 'VCW' : 'X', 'DWW' : 'X', 'MHG' : 'X', 'HSC' : 'X', 'ACG' : 'T', 'SNC' : 'X', 'HBB' : 'X', 'DNG' : 'X', 
'BDW' : 'X', 'VBY' : 'X', 'TSV' : 'X', 'NWR' : 'X', 'BGC' : 'X', 'BBH' : 'X', 'TSN' : 'X', 'ATA' : 'I', 'NTN' : 'X', 'BBM' : 'X', 
'VGD' : 'X', 'KDG' : 'X', 'TRC' : 'X', 'KGT' : 'X', 'AGN' : 'X', 'GKB' : 'X', 'RCB' : 'X', 'GVD' : 'X', 'BKD' : 'X', 'KBM' : 'X', 
'HCG' : 'X', 'BTY' : 'X', 'SMA' : 'X', 'KAK' : 'X', 'VKN' : 'X', 'BWH' : 'X', 'BSA' : 'X', 'MBW' : 'X', 'BCS' : 'X', 'HCS' : 'X', 
'CWA' : 'X', 'RCN' : 'X', 'TVR' : 'X', 'MWH' : 'X', 'MYM' : 'X', 'MGA' : 'R', 'KDC' : 'X', 'NYN' : 'X', 'DNW' : 'X', 'MKM' : 'X', 
'YGD' : 'X', 'RDM' : 'X', 'CDN' : 'X', 'BWY' : 'X', 'CKT' : 'X', 'HDS' : 'X', 'DHY' : 'X', 'CCT' : 'P', 'WRA' : 'X', 'KVH' : 'X', 
'CAA' : 'Q', 'HMK' : 'X', 'NSD' : 'X', 'RWS' : 'X', 'ADK' : 'X', 'ACR' : 'T', 'HRB' : 'X', 'SRK' : 'X', 'THA' : 'X', 'BHG' : 'X', 
'GSD' : 'X', 'DWK' : 'X', 'ANT' : 'X', 'CBC' : 'X', 'WVB' : 'X', 'SBV' : 'X', 'KST' : 'X', 'DKW' : 'X', 'VVM' : 'X', 'HKB' : 'X', 
'HWG' : 'X', 'VHR' : 'X', 'AAT' : 'N', 'RSR' : 'X', 'VTV' : 'X', 'NYK' : 'X', 'TNM' : 'X', 'GRG' : 'X', 'DSH' : 'X', 'AVA' : 'X', 
'TWS' : 'X', 'NRM' : 'X', 'GRR' : 'X', 'ADG' : 'X', 'YKR' : 'X', 'BNV' : 'X', 'SRB' : 'X', 'HKV' : 'X', 'KHW' : 'X', 'DRB' : 'X', 
'CTM' : 'L', 'KCY' : 'X', 'YHB' : 'X', 'VVD' : 'X', 'RYG' : 'X', 'GDR' : 'X', 'YHH' : 'X', 'MRK' : 'X', 'CTV' : 'L', 'GMG' : 'X', 
'GGM' : 'G', 'WRD' : 'X', 'DAS' : 'X', 'AYD' : 'X', 'SGH' : 'X', 'NCB' : 'X', 'SNK' : 'X', 'HHD' : 'X', 'VKD' : 'X', 'MKR' : 'X', 
'CYR' : 'X', 'DSM' : 'X', 'TMC' : 'X', 'YBC' : 'X', 'GWB' : 'X', 'YMY' : 'X', 'DMB' : 'X', 'GDW' : 'X', 'MNC' : 'X', 'MDC' : 'X', 
'KDW' : 'X', 'MMN' : 'X', 'KKR' : 'X', 'TGT' : 'C', 'RBG' : 'X', 'TVB' : 'X', 'VWW' : 'X', 'DHB' : 'X', 'KTW' : 'X', 'WDK' : 'X', 
'YGN' : 'X', 'WST' : 'X', 'SWV' : 'X', 'MGK' : 'X', 'DMN' : 'X', 'CDH' : 'X', 'BTC' : 'X', 'DWS' : 'X', 'HWS' : 'X', 'VWC' : 'X', 
'GMC' : 'X', 'SRC' : 'X', 'STY' : 'X', 'CRD' : 'X', 'GVV' : 'X', 'NCC' : 'X', 'STS' : 'X', 'BTN' : 'X', 'MSD' : 'X', 'MTY' : 'X', 
'RDA' : 'X', 'THR' : 'X', 'DHC' : 'X', 'BYD' : 'X', 'WNK' : 'X', 'DCM' : 'X', 'GBC' : 'X', 'YCG' : 'X', 'HSG' : 'X', 'HVY' : 'X', 
'HHW' : 'X', 'KGV' : 'X', 'WDS' : 'X', 'RGS' : 'X', 'YHY' : 'X', 'AKR' : 'X', 'MKH' : 'X', 'RWC' : 'X', 'MHR' : 'X', 'CGS' : 'R', 
'BTK' : 'X', 'ABG' : 'X', 'ABN' : 'X', 'BGS' : 'X', 'SNR' : 'X', 'WKB' : 'X', 'TRD' : 'X', 'AAA' : 'K', 'KDA' : 'X', 'SSN' : 'X', 
'RKG' : 'X', 'KKB' : 'X', 'YYB' : 'X', 'BWK' : 'X', 'CBV' : 'X', 'MBR' : 'X', 'YYS' : 'X', 'CHR' : 'X', 'KVM' : 'X', 'BCD' : 'X', 
'BDC' : 'X', 'TYS' : 'X', 'KAH' : 'X', 'ABA' : 'X', 'DHK' : 'X', 'VTM' : 'X', 'RVH' : 'X', 'HHA' : 'X', 'HGV' : 'X', 'WBB' : 'X', 
'WDN' : 'X', 'STH' : 'X', 'BTH' : 'X', 'SBA' : 'X', 'MND' : 'X', 'KSR' : 'X', 'WAC' : 'X', 'YDK' : 'X', 'MHS' : 'X', 'CWW' : 'X', 
'BBS' : 'X', 'YAD' : 'X', 'YRK' : 'X', 'DHM' : 'X', 'SCY' : 'X', 'GNM' : 'X', 'RCR' : 'X', 'MST' : 'X', 'SYW' : 'X', 'SDG' : 'X', 
'DVD' : 'X', 'ADD' : 'X', 'AAK' : 'X', 'KAD' : 'X', 'HYH' : 'X', 'BHS' : 'X', 'AWK' : 'X', 'HTY' : 'X', 'KTM' : 'X', 'BGW' : 'X', 
'SBW' : 'X', 'CWG' : 'X', 'GAC' : 'D', 'HRS' : 'X', 'MRG' : 'X', 'AVC' : 'X', 'YCK' : 'X', 'GNV' : 'X', 'DBD' : 'X', 'YNC' : 'X', 
'BAB' : 'X', 'DTG' : 'X', 'BAS' : 'X', 'YKY' : 'X', 'DCN' : 'X', 'TBA' : 'X', 'CWN' : 'X', 'YBD' : 'X', 'AKY' : 'X', 'GVM' : 'X', 
'WMT' : 'X', 'VTR' : 'X', 'DBA' : 'X', 'DRA' : 'X', 'SGM' : 'X', 'GCW' : 'A', 'RHC' : 'X', 'YMA' : 'X', 'MKW' : 'X', 'CVW' : 'X', 
'BVW' : 'X', 'CAW' : 'X', 'SSK' : 'X', 'HBW' : 'X', 'NBH' : 'X', 'DCR' : 'X', 'SDC' : 'X', 'HVM' : 'X', 'KBC' : 'X', 'WTC' : 'X', 
'BRV' : 'X', 'MBH' : 'X', 'TRW' : 'X', 'RCV' : 'X', 'KDY' : 'X', 'DWV' : 'X', 'VKH' : 'X', 'SCN' : 'X', 'TWN' : 'X', 'CSA' : 'X', 
'NWN' : 'X', 'VDV' : 'X', 'RAM' : 'X', 'BMK' : 'X', 'DCS' : 'X', 'HRD' : 'X', 'WAB' : 'X', 'CRV' : 'X', 'NVV' : 'X', 'CVG' : 'X', 
'TBK' : 'X', 'TCH' : 'S', 'WKY' : 'X', 'YWS' : 'X', 'RBK' : 'X', 'VDD' : 'X', 'TBY' : 'X', 'KYS' : 'X', 'RHS' : 'X', 'HDD' : 'X', 
'CBM' : 'X', 'AYT' : 'X', 'WGN' : 'X', 'CVR' : 'X', 'RMS' : 'X', 'VYW' : 'X', 'MKV' : 'X', 'WNV' : 'X', 'CSB' : 'X', 'SDN' : 'X', 
'CCK' : 'P', 'MYD' : 'X', 'AYN' : 'X', 'ATM' : 'I', 'YVA' : 'X', 'BDA' : 'X', 'WMA' : 'X', 'TMV' : 'X', 'AVW' : 'X', 'BHA' : 'X', 
'CRA' : 'X', 'WVM' : 'X', 'SCD' : 'X', 'HBG' : 'X', 'WAM' : 'X', 'SRA' : 'X', 'SWM' : 'X', 'RNS' : 'X', 'YCY' : 'X', 'VBK' : 'X', 
'RDC' : 'X', 'KHB' : 'X', 'TSC' : 'X', 'GRV' : 'X', 'SHC' : 'X', 'VBH' : 'X', 'NHM' : 'X', 'SVC' : 'X', 'CCG' : 'P', 'BWG' : 'X', 
'WSV' : 'X', 'WCG' : 'X', 'VGT' : 'X', 'DGN' : 'X', 'YSG' : 'X', 'KDM' : 'X', 'CYT' : 'X', 'SRG' : 'X', 'VAK' : 'X', 'AAS' : 'X', 
'VMM' : 'X', 'GSS' : 'X', 'VHH' : 'X', 'KNN' : 'X', 'NMK' : 'X', 'CBN' : 'X', 'YWN' : 'X', 'DAA' : 'X', 'KTT' : 'X', 'RRR' : 'X', 
'SVB' : 'X', 'HMB' : 'X', 'TTS' : 'X', 'SBR' : 'X', 'VKA' : 'X', 'GNB' : 'X', 'WWM' : 'X', 'WYC' : 'X', 'TAA' : None, 'DBR' : 'X', 
'HNN' : 'X', 'KKT' : 'X', 'SBB' : 'X', 'DYD' : 'X', 'BBY' : 'X', 'MCT' : 'X', 'TRN' : 'X', 'DCD' : 'X', 'DBK' : 'X', 'HAR' : 'X', 
'VSR' : 'X', 'WWA' : 'X', 'RGR' : 'X', 'KKM' : 'X', 'HSB' : 'X', 'RDB' : 'X', 'YBH' : 'X', 'SST' : 'X', 'TNW' : 'X', 'YTY' : 'X', 
'AHR' : 'X', 'MWC' : 'X', 'WGR' : 'X', 'ADY' : 'X', 'CSD' : 'X', 'MDY' : 'X', 'NMY' : 'X', 'TAV' : 'X', 'WAA' : 'X', 'VSH' : 'X', 
'TNR' : 'X', 'MYC' : 'X', 'WMS' : 'X', 'CTC' : 'L', 'YAK' : 'X', 'YCA' : 'X', 'DYM' : 'X', 'SNH' : 'X', 'AMK' : 'X', 'SMB' : 'X', 
'SYV' : 'X', 'YHR' : 'X', 'SAC' : 'X', 'DCV' : 'X', 'CNH' : 'X', 'GGR' : 'G', 'HDA' : 'X', 'BYK' : 'X', 'YVY' : 'X', 'HGC' : 'X', 
'KAR' : 'X', 'KCT' : 'X', 'SMK' : 'X', 'ATT' : 'I', 'AHB' : 'X', 'WCN' : 'X', 'VSC' : 'X', 'HBM' : 'X', 'NAA' : 'X', 'GMS' : 'X', 
'RVT' : 'X', 'BRM' : 'X', 'AWM' : 'X', 'NVT' : 'X', 'DCC' : 'X', 'YHG' : 'X', 'HSA' : 'X', 'YVN' : 'X', 'BAW' : 'X', 'GRC' : 'X', 
'SCG' : 'X', 'MRW' : 'X', 'CKN' : 'X', 'BAN' : 'X', 'YVT' : 'X', 'SDD' : 'X', 'BBK' : 'X', 'GDS' : 'X', 'YAT' : 'X', 'CHA' : 'X', 
'TTT' : 'F', 'WDD' : 'X', 'CWB' : 'X', 'TDM' : 'X', 'NGD' : 'X', 'WGD' : 'X', 'DVS' : 'X', 'MAY' : 'X', 'BRY' : 'X', 'BVC' : 'X', 
'CBH' : 'X', 'RMB' : 'X', 'DBH' : 'X', 'RGW' : 'X', 'BGB' : 'X', 'RWW' : 'X', 'WWY' : 'X', 'DWM' : 'X', 'SNB' : 'X', 'BCY' : 'X', 
'BVY' : 'X', 'RVG' : 'X', 'KVW' : 'X', 'VKW' : 'X', 'RYW' : 'X', 'NHD' : 'X', 'TWK' : 'X', 'MNW' : 'X', 'TVM' : 'X', 'MCN' : 'X', 
'TKK' : 'X', 'KKY' : 'X', 'BTD' : 'X', 'HVH' : 'X', 'YNA' : 'X', 'WGG' : 'X', 'GKD' : 'X', 'DWC' : 'X', 'HBN' : 'X', 'DRT' : 'X', 
'SDT' : 'X', 'BKT' : 'X', 'SYY' : 'X', 'TDA' : 'X', 'VBM' : 'X', 'BKM' : 'X', 'AVK' : 'X', 'TSW' : 'X', 'NRB' : 'X', 'CAG' : 'Q', 
'NGG' : 'X', 'ASB' : 'X', 'MWN' : 'X', 'NRN' : 'X', 'SKD' : 'X', 'WKM' : 'X', 'BHV' : 'X', 'MVY' : 'X', 'NWB' : 'X', 'NSC' : 'X', 
'YGC' : 'X', 'ASM' : 'X', 'RGN' : 'X', 'GYD' : 'X', 'HBY' : 'X', 'KGD' : 'X', 'BAC' : 'X', 'SGK' : 'X', 'ADM' : 'X', 'GHT' : 'X', 
'YTT' : 'X', 'TSG' : 'X', 'HSK' : 'X', 'CRM' : 'X', 'YKS' : 'X', 'AWA' : 'X', 'ARA' : 'X', 'DHT' : 'X', 'SKY' : 'X', 'KAS' : 'X', 
'CDD' : 'X', 'MNT' : 'X', 'DWG' : 'X', 'DGH' : 'X', 'MTK' : 'X', 'DBT' : 'X', 'MNM' : 'X', 'NHH' : 'X', 'RSB' : 'X', 'RWB' : 'X', 
'HMV' : 'X', 'GKY' : 'X', 'HDN' : 'X', 'SSD' : 'X', 'RNM' : 'X', 'ASV' : 'X', 'YCV' : 'X', 'DKK' : 'X', 'RBS' : 'X', 'SAS' : 'X', 
'VBV' : 'X', 'SNM' : 'X', 'GMD' : 'X', 'NCK' : 'X', 'MVW' : 'X', 'RMT' : 'X', 'RSW' : 'X', 'NGC' : 'X', 'WVW' : 'X', 'RBA' : 'X', 
'RMN' : 'X', 'NTG' : 'X', 'VNR' : 'X', 'KDK' : 'X', 'MRN' : 'X', 'BWR' : 'X', 'NTW' : 'X', 'AAM' : 'X', 'NAB' : 'X', 'SBY' : 'X', 
'WHV' : 'X', 'SRR' : 'X', 'WYR' : 'X', 'SWY' : 'X', 'DKA' : 'X', 'KBB' : 'X', 'AMY' : 'X', 'GAW' : 'X', 'GMA' : 'X', 'WVA' : 'X', 
'DTM' : 'X', 'DSY' : 'X', 'DSR' : 'X', 'YYM' : 'X', 'SYA' : 'X', 'VKG' : 'X', 'CKA' : 'X', 'AMM' : 'X', 'RYH' : 'X', 'BCV' : 'X', 
'ATK' : 'X', 'HDH' : 'X', 'KYA' : 'X', 'CGT' : 'R', 'TRT' : 'X', 'VSS' : 'X', 'KTA' : 'X', 'BVA' : 'X', 'BMR' : 'X', 'TSS' : 'X', 
'BHD' : 'X', 'GYB' : 'X', 'SSH' : 'X', 'WCH' : 'X', 'BBR' : 'X', 'VNT' : 'X', 'WBY' : 'X', 'YNH' : 'X', 'CDG' : 'X', 'DSK' : 'X', 
'NWA' : 'X', 'AGA' : 'R', 'TGM' : 'X', 'DNR' : 'X', 'CCS' : 'P', 'MMB' : 'X', 'NVC' : 'X', 'GAM' : 'X', 'KMD' : 'X', 'WGW' : 'X', 
'DNS' : 'X', 'HAK' : 'X', 'NTH' : 'X', 'VKB' : 'X', 'CGW' : 'R', 'WGK' : 'X', 'SRH' : 'X', 'AAD' : 'X', 'WHC' : 'X', 'GHA' : 'X', 
'NDK' : 'X', 'TNY' : 'X', 'BSN' : 'X', 'GYV' : 'X', 'GDH' : 'X', 'WRC' : 'X', 'BKH' : 'X', 'BRG' : 'X', 'KNV' : 'X', 'SHT' : 'X', 
'NKH' : 'X', 'VSA' : 'X', 'YGR' : 'X', 'YGG' : 'X', 'TNS' : 'X', 'HTA' : 'X', 'AHT' : 'X', 'TDY' : 'X', 'CGH' : 'R', 'YYH' : 'X', 
'KCA' : 'X', 'DTR' : 'X', 'MGS' : 'X', 'YBK' : 'X', 'NRD' : 'X', 'ANY' : 'X', 'NYA' : 'X', 'CAY' : 'H', 'NYW' : 'X', 'AAH' : 'X', 
'AHA' : 'X', 'WND' : 'X', 'KNH' : 'X', 'GWR' : 'X', 'RKM' : 'X', 'VCT' : 'X', 'DYG' : 'X', 'BNS' : 'X', 'SAT' : 'X', 'WBW' : 'X', 
'WDV' : 'X', 'WTS' : 'X', 'RRC' : 'X', 'TAG' : None, 'MNG' : 'X', 'MYV' : 'X', 'TMR' : 'X', 'ACA' : 'T', 'NKB' : 'X', 'RRA' : 'X', 
'KNY' : 'X', 'KYM' : 'X', 'DKD' : 'X', 'BRC' : 'X', 'NKS' : 'X', 'YCM' : 'X', 'DHG' : 'X', 'SKK' : 'X', 'RYR' : 'X', 'TYB' : 'X', 
'TBW' : 'X', 'THG' : 'X', 'RAV' : 'X', 'TRS' : 'X', 'GBD' : 'X', 'YAS' : 'X', 'CSS' : 'X', 'MSY' : 'X', 'ABT' : 'X', 'DCY' : 'X', 
'ARV' : 'X', 'BTS' : 'X', 'VHB' : 'X', 'SKS' : 'X', 'ATW' : 'I', 'SKB' : 'X', 'NKW' : 'X', 'YCW' : 'X', 'MSH' : 'X', 'KYV' : 'X', 
'GTK' : 'V', 'YCS' : 'X', 'NVA' : 'X', 'CKY' : 'X', 'YRY' : 'X', 'DDC' : 'X', 'MRR' : 'X', 'CNS' : 'X', 'DHD' : 'X', 'YAY' : 'X', 
'HRK' : 'X', 'HNR' : 'X', 'WRT' : 'X', 'SMY' : 'X', 'GTY' : 'V', 'KVC' : 'X', 'TVS' : 'X', 'GKT' : 'X', 'RTG' : 'X', 'ARY' : 'X', 
'GND' : 'X', 'WWB' : 'X', 'NSB' : 'X', 'VTN' : 'X', 'AMS' : 'X', 'WBG' : 'X', 'SVH' : 'X', 'GVB' : 'X', 'RDK' : 'X', 'BNT' : 'X', 
'CDC' : 'X', 'BDD' : 'X', 'SYS' : 'X', 'RKY' : 'X', 'RHW' : 'X', 'GKH' : 'X', 'GCT' : 'A', 'GYG' : 'X', 'GTT' : 'V', 'MAV' : 'X', 
'TSY' : 'X', 'AGY' : 'S', 'YKN' : 'X', 'VHC' : 'X', 'ABD' : 'X', 'HTC' : 'X', 'YYK' : 'X', 'GGT' : 'G', 'HYN' : 'X', 'HCV' : 'X', 
'NNM' : 'X', 'AHK' : 'X', 'KHR' : 'X', 'TVK' : 'X', 'HYT' : 'X', 'MWG' : 'X', 'RWR' : 'X', 'VAG' : 'X', 'VHS' : 'X', 'HCY' : 'X', 
'YAM' : 'X', 'AGM' : 'X', 'YMT' : 'X', 'BBW' : 'X', 'KWR' : 'X', 'HBC' : 'X', 'DHR' : 'X', 'NDC' : 'X', 'TSK' : 'X', 'VRG' : 'X', 
'TCM' : 'S', 'HCT' : 'X', 'HHC' : 'X', 'BCW' : 'X', 'HKW' : 'X', 'WKR' : 'X', 'HSS' : 'X', 'YVW' : 'X', 'KBR' : 'X', 'ARG' : 'X', 
'TVW' : 'X', 'WAD' : 'X', 'AKN' : 'X', 'KAV' : 'X', 'HDR' : 'X', 'CMS' : 'X', 'HHV' : 'X', 'AAG' : 'K', 'WYN' : 'X', 'HTB' : 'X', 
'DBN' : 'X', 'VWD' : 'X', 'RHT' : 'X', 'YSH' : 'X', 'WTB' : 'X', 'BYY' : 'X', 'DAC' : 'X', 'KYD' : 'X', 'HYC' : 'X', 'VWR' : 'X', 
'SNN' : 'X', 'SWN' : 'X', 'RYB' : 'X', 'HGT' : 'X', 'RNK' : 'X', 'SND' : 'X', 'KWK' : 'X', 'WSB' : 'X', 'NRA' : 'X', 'VVB' : 'X', 
'GDY' : 'X', 'RBT' : 'X', 'CNN' : 'X', 'VDS' : 'X', 'WCR' : 'X', 'BST' : 'X', 'MMK' : 'X', 'MDK' : 'X', 'CNC' : 'X', 'VAA' : 'X', 
'CYS' : 'X', 'HNY' : 'X', 'BCR' : 'X', 'WRM' : 'X', 'WGC' : 'X', 'WWG' : 'X', 'NHV' : 'X', 'WGS' : 'X', 'HDT' : 'X', 'SMS' : 'X', 
'GGH' : 'G', 'AWY' : 'X', 'CCC' : 'P', 'GNR' : 'X', 'VRT' : 'X', 'SMG' : 'X', 'TMD' : 'X', 'YVG' : 'X', 'SYK' : 'X', 'TKC' : 'X', 
'SNS' : 'X', 'MDR' : 'X', 'VVV' : 'X', 'GNN' : 'X', 'GNW' : 'X', 'HKG' : 'X', 'WKN' : 'X', 'NSN' : 'X', 'WMY' : 'X', 'GRY' : 'X', 
'MRM' : 'X', 'NGW' : 'X', 'TWR' : 'X', 'GYS' : 'X', 'GBR' : 'X', 'SMD' : 'X', 'DGV' : 'X', 'SGB' : 'X', 'NGT' : 'X', 'AGW' : 'X', 
'VBG' : 'X', 'NMC' : 'X', 'GKW' : 'X', 'GHG' : 'X', 'TCC' : 'S', 'KCW' : 'X', 'DNN' : 'X', 'HNK' : 'X', 'SAA' : 'X', 'KYN' : 'X', 
'VHY' : 'X', 'YTV' : 'X', 'CRH' : 'X', 'AAC' : 'N', 'VCR' : 'X', 'MAN' : 'X', 'WWC' : 'X', 'BKW' : 'X', 'AAR' : 'K', 'RBN' : 'X', 
'TTC' : 'F', 'GMW' : 'X', 'WAS' : 'X', 'NSM' : 'X', 'VCK' : 'X', 'WGH' : 'X', 'BYB' : 'X', 'NBD' : 'X', 'BDT' : 'X', 'NHT' : 'X', 
'BSV' : 'X', 'KKK' : 'X', 'YRA' : 'X', 'KNT' : 'X', 'KKA' : 'X', 'YGM' : 'X', 'VNM' : 'X', 'HBH' : 'X', 'DRV' : 'X', 'VGA' : 'X', 
'RKW' : 'X', 'RKT' : 'X', 'AKG' : 'X', 'VRC' : 'X', 'BGA' : 'X', 'VDG' : 'X', 'MCM' : 'X', 'YDV' : 'X', 'TGV' : 'X', 'AAW' : 'X', 
'BTV' : 'X', 'BRR' : 'X', 'KKD' : 'X', 'BGR' : 'X', 'VDR' : 'X', 'ABY' : 'X', 'NDA' : 'X', 'TWG' : 'X', 'HKR' : 'X', 'KBG' : 'X', 
'WNM' : 'X', 'KCK' : 'X', 'CYH' : 'X', 'NHC' : 'X', 'VBC' : 'X', 'KCC' : 'X', 'YNS' : 'X', 'HTG' : 'X', 'BYC' : 'X', 'YYA' : 'X', 
'KWY' : 'X', 'KWD' : 'X', 'DKV' : 'X', 'BDS' : 'X', 'GTA' : 'V', 'MRD' : 'X', 'RKS' : 'X', 'DCG' : 'X', 'CWY' : 'X', 'GVW' : 'X', 
'HVR' : 'X', 'DYW' : 'X', 'YTR' : 'L', 'TVV' : 'X', 'WGB' : 'X', 'CHD' : 'X', 'HKH' : 'X', 'CMK' : 'X', 'WVY' : 'X', 'KCS' : 'X', 
'KNC' : 'X', 'VHD' : 'X', 'SNG' : 'X', 'YGV' : 'X', 'SVV' : 'X', 'VNC' : 'X', 'CNT' : 'X', 'RGD' : 'X', 'NWT' : 'X', 'TDG' : 'X', 
'DRW' : 'X', 'VNH' : 'X', 'RYN' : 'X', 'AGB' : 'X', 'VRD' : 'X', 'BMT' : 'X', 'TKY' : 'X', 'AHS' : 'X', 'CTH' : 'L', 'TTA' : 'L', 
'TRB' : 'X', 'SSG' : 'X', 'HWV' : 'X', 'HWH' : 'X', 'GNC' : 'X', 'RHG' : 'X', 'DGY' : 'X', 'KKS' : 'X', 'RWD' : 'X', 'RSC' : 'X', 
'STV' : 'X', 'SRN' : 'X', 'RMY' : 'X', 'YYC' : 'X', 'NAR' : 'X', 'WYM' : 'X', 'CCA' : 'P', 'KMG' : 'X', 'SCH' : 'X', 'NWS' : 'X', 
'GRN' : 'X', 'HKM' : 'X', 'CRB' : 'X', 'NNN' : 'X', 'WDA' : 'X', 'BMY' : 'X', 'KAA' : 'X', 'HNV' : 'X', 'TDC' : 'X', 'CAB' : 'X', 
'DSS' : 'X', 'ASA' : 'X', 'GWH' : 'X', 'CRY' : 'X', 'HYB' : 'X', 'DVW' : 'X', 'VTA' : 'X', 'DTN' : 'X', 'KSS' : 'X', 'BAM' : 'X', 
'SCB' : 'X', 'WWV' : 'X', 'AWG' : 'X', 'RAW' : 'X', 'ABK' : 'X', 'WNG' : 'X', 'SAN' : 'X', 'HYG' : 'X', 'NMM' : 'X', 'YAA' : 'X', 
'VCD' : 'X', 'NNG' : 'X', 'VTH' : 'X', 'RAS' : 'X', 'RNT' : 'X', 'WNY' : 'X', 'AND' : 'X', 'VGN' : 'X', 'RCY' : 'X', 'VST' : 'X', 
'BCK' : 'X', 'DGS' : 'X', 'MVC' : 'X', 'RRK' : 'X', 'KKV' : 'X', 'RYS' : 'X', 'YRD' : 'X', 'GDN' : 'X', 'HTD' : 'X', 'DMH' : 'X', 
'AYY' : 'X', 'HAT' : 'X', 'RKA' : 'X', 'RDD' : 'X', 'YVH' : 'X', 'CKG' : 'X', 'BMV' : 'X', 'TGB' : 'X', 'VGB' : 'X', 'YHC' : 'X', 
'AWT' : 'X', 'GKM' : 'X', 'NKY' : 'X', 'RVY' : 'X', 'WCS' : 'X', 'WVC' : 'X', 'WCY' : 'X', 'NND' : 'X', 'DDY' : 'X', 'VTY' : 'X', 
'TAW' : 'X', 'NDW' : 'X', 'GVY' : 'X', 'NAH' : 'X', 'RRV' : 'X', 'RGK' : 'X', 'MBS' : 'X', 'HYK' : 'X', 'WAY' : 'X', 'HDB' : 'X', 
'MAA' : 'X', 'DMV' : 'X', 'VCG' : 'X', 'SYG' : 'X', 'DRH' : 'X', 'DTC' : 'X', 'TYD' : 'X', 'RYC' : 'X', 'TMA' : 'X', 'NBV' : 'X', 
'CDM' : 'X', 'WDY' : 'X', 'RHM' : 'X', 'NDM' : 'X', 'WTN' : 'X', 'CNV' : 'X', 'CYD' : 'X', 'TNN' : 'X', 'VHV' : 'X', 'RMK' : 'X', 
'MYB' : 'X', 'VKS' : 'X', 'MRT' : 'X', 'NMB' : 'X', 'WNB' : 'X', 'BHH' : 'X', 'YKG' : 'X', 'YNB' : 'X', 'KVT' : 'X', 'CYW' : 'X', 
'MBG' : 'X', 'KTN' : 'X', 'ARC' : 'X', 'VGW' : 'X', 'DVA' : 'X', 'GYK' : 'X', 'BAR' : 'X', 'WBK' : 'X', 'NYC' : 'X', 'GBW' : 'X', 
'HCB' : 'X', 'SKW' : 'X', 'DNA' : 'X', 'DDR' : 'X', 'RVA' : 'X', 'VGV' : 'X', 'DMG' : 'X', 'RAB' : 'X', 'MAK' : 'X', 'RSH' : 'X', 
'WNH' : 'X', 'AHM' : 'X', 'MGH' : 'X', 'KMY' : 'X', 'YRT' : 'X', 'GTS' : 'V', 'WBR' : 'X', 'KVV' : 'X', 'HMC' : 'X', 'CVN' : 'X', 
'TTW' : 'X', 'RBD' : 'X', 'DKC' : 'X', 'RBV' : 'X', 'CNY' : 'X', 'SCT' : 'X', 'HWW' : 'X', 'WYV' : 'X', 'GKS' : 'X', 'RWM' : 'X', 
'BVD' : 'X', 'CRK' : 'X', 'GTH' : 'V', 'CHK' : 'X', 'MAG' : 'X', 'RTT' : 'X', 'CKS' : 'X', 'TKB' : 'X', 'WVR' : 'X', 'MBK' : 'X', 
'VSW' : 'X', 'YBM' : 'X', 'VYN' : 'X', 'YBG' : 'X', 'YWG' : 'X', 'DCA' : 'X', 'RKH' : 'X', 'HYA' : 'X', 'YTM' : 'X', 'GAK' : 'X', 
'AMB' : 'X', 'HBK' : 'X', 'TNK' : 'X', 'RAT' : 'X', 'AMH' : 'X', 'WSY' : 'X', 'WBS' : 'X', 'SKC' : 'X', 'YBR' : 'X', 'DGW' : 'X', 
'DGM' : 'X', 'NCM' : 'X', 'TMT' : 'X', 'WRY' : 'X', 'WKW' : 'X', 'AWB' : 'X', 'YNT' : 'X', 'BKN' : 'X', 'SDY' : 'X', 'DAK' : 'X', 
'RBM' : 'X', 'CYA' : 'X', 'DNH' : 'X', 'HYS' : 'X', 'HNA' : 'X', 'DAV' : 'X', 'DYY' : 'X', 'CSV' : 'X', 'WDT' : 'X', 'DBY' : 'X', 
'MVM' : 'X', 'TMG' : 'X', 'GBA' : 'X', 'YNY' : 'X', 'WYA' : 'X', 'RNH' : 'X', 'VSV' : 'X', 'CMN' : 'X', 'SYB' : 'X', 'DVM' : 'X', 
'SGT' : 'X', 'WRS' : 'X', 'BAK' : 'X', 'KTS' : 'X', 'WYS' : 'X', 'NGK' : 'X', 'CTK' : 'L', 'MNV' : 'X', 'KRA' : 'X', 'YAW' : 'X', 
'NNR' : 'X', 'GNS' : 'X', 'SKN' : 'X', 'HRN' : 'X', 'YKD' : 'X', 'MGY' : 'X', 'NKD' : 'X', 'BNN' : 'X', 'BDM' : 'X', 'MBM' : 'X', 
'WWS' : 'X', 'KSG' : 'X', 'DMR' : 'X', 'NST' : 'X', 'KNA' : 'X', 'DHV' : 'X', 'SYC' : 'X', 'CNR' : 'X', 'TRK' : 'X', 'WTT' : 'X', 
'WHR' : 'X', 'GMK' : 'X', 'VMR' : 'X', 'MWY' : 'X', 'GTB' : 'V', 'AVR' : 'X', 'VMA' : 'X', 'DDH' : 'X', 'BSD' : 'X', 'HCC' : 'X', 
'NCA' : 'X', 'VBS' : 'X', 'CHY' : 'X', 'TRA' : None, 'SKM' : 'X', 'BYN' : 'X', 'CAR' : 'Q', 'MVR' : 'X', 'YAC' : 'X', 'NGA' : 'X', 
'KRG' : 'X', 'MVG' : 'X', 'DMK' : 'X', 'SKA' : 'X', 'CSR' : 'X', 'RTY' : 'X', 'CMC' : 'X', 'RBY' : 'X', 'SRD' : 'X', 'RHH' : 'X', 
'GKK' : 'X', 'KKC' : 'X', 'GAS' : 'X', 'HRR' : 'X', 'BMD' : 'X', 'GVG' : 'X', 'GDC' : 'X', 'HDC' : 'X', 'GMN' : 'X', 'TWW' : 'X', 
'NNW' : 'X', 'KAN' : 'X', 'CDS' : 'X', 'VWG' : 'X', 'GBM' : 'X', 'WCB' : 'X', 'BBB' : 'X', 'TSR' : 'X', 'RMV' : 'X', 'NTR' : 'X', 
'VVS' : 'X', 'WNA' : 'X', 'TMB' : 'X', 'BVB' : 'X', 'STB' : 'X', 'HKK' : 'X', 'DNB' : 'X', 'BYW' : 'X', 'VKY' : 'X', 'NMH' : 'X', 
'GCM' : 'A', 'NCV' : 'X', 'GRM' : 'X', 'MDM' : 'X', 'ANB' : 'X', 'NNK' : 'X', 'NSY' : 'X', 'BMH' : 'X', 'SSR' : 'X', 'GHC' : 'X', 
'NRT' : 'X', 'WDW' : 'X', 'NRR' : 'X', 'DVG' : 'X', 'KNR' : 'X', 'CMM' : 'X', 'NBY' : 'X', 'GKR' : 'X', 'CRG' : 'X', 'RCD' : 'X', 
'WSA' : 'X', 'VWA' : 'X', 'NSW' : 'X', 'AVN' : 'X', 'BRD' : 'X', 'VAH' : 'X', 'CTG' : 'L', 'TDH' : 'X', 'GWM' : 'X', 'DHW' : 'X', 
'YGY' : 'X', 'YSM' : 'X', 'WAK' : 'X', 'TYA' : 'X', 'RRH' : 'X', 'VMN' : 'X', 'VSB' : 'X', 'YSB' : 'X', 'ACC' : 'T', 'MSB' : 'X', 
'NWM' : 'X', 'HGD' : 'X', 'HAG' : 'X', 'STA' : 'X', 'SCM' : 'X', 'ANR' : 'X', 'SBC' : 'X', 'TBR' : 'X', 'NVK' : 'X', 'WGT' : 'X', 
'HST' : 'X', 'RKV' : 'X', 'RNW' : 'X', 'NBW' : 'X', 'MHY' : 'X', 'DCK' : 'X', 'VAS' : 'X', 'TNT' : 'X', 'AMG' : 'X', 'MRY' : 'X', 
'KCB' : 'X', 'MCR' : 'X', 'VHW' : 'X', 'RAD' : 'X', 'ASN' : 'X', 'MCH' : 'X', 'AGC' : 'S', 'BTT' : 'X', 'NVG' : 'X', 'BVV' : 'X', 
'ATH' : 'I', 'WSN' : 'X', 'YSR' : 'X', 'ADN' : 'X', 'RVR' : 'X', 'WHY' : 'X', 'NTB' : 'X', 'KRT' : 'X', 'DBS' : 'X', 'WSC' : 'X', 
'TWH' : 'X', 'BDN' : 'X', 'DCT' : 'X', 'RMC' : 'X', 'WTV' : 'X', 'HHN' : 'X', 'YAV' : 'X', 'WCT' : 'X', 'TVA' : 'X', 'VKK' : 'X', 
'CTW' : 'L', 'VRR' : 'X', 'MBA' : 'X', 'YNK' : 'X', 'DTW' : 'X', 'CYM' : 'X', 'HTW' : 'X', 'NSS' : 'X', 'RHR' : 'X', 'YAH' : 'X', 
'NNB' : 'X', 'NMG' : 'X', 'YVV' : 'X', 'RWG' : 'X', 'SRT' : 'X', 'WCW' : 'X', 'GSN' : 'X', 'YHN' : 'X', 'RNA' : 'X', 'ADC' : 'X', 
'GYM' : 'X', 'KMS' : 'X', 'HSY' : 'X', 'YWT' : 'X', 'MGN' : 'X', 'WCA' : 'X', 'TWA' : 'X', 'THM' : 'X', 'MGD' : 'X', 'YAB' : 'X', 
'ADS' : 'X', 'MCV' : 'X', 'SKV' : 'X', 'NYM' : 'X', 'WBT' : 'X', 'KAG' : 'X', 'MAC' : 'X', 'KCH' : 'X', 'TCT' : 'S', 'YMW' : 'X', 
'SBN' : 'X', 'NGM' : 'X', 'NHA' : 'X', 'MCS' : 'X', 'TKD' : 'X', 'BGV' : 'X', 'RTD' : 'X', 'HAA' : 'X', 'MMT' : 'X', 'DRC' : 'X', 
'BWV' : 'X', 'THW' : 'X', 'BBT' : 'X', 'NRH' : 'X', 'CCD' : 'P', 'AHG' : 'X', 'GCC' : 'A', 'AGH' : 'X', 'RYD' : 'X', 'KWW' : 'X', 
'YMG' : 'X', 'MGW' : 'X', 'TNV' : 'X', 'VBB' : 'X', 'GWV' : 'X', 'DTV' : 'X', 'AYR' : 'X', 'WAW' : 'X', 'AMA' : 'X', 'KDB' : 'X', 
'MWM' : 'X', 'RVW' : 'X', 'CHM' : 'X', 'RVN' : 'X', 'GTG' : 'V', 'CBR' : 'X', 'HMR' : 'X', 'BSR' : 'X', 'KGH' : 'X', 'AMV' : 'X', 
'WRK' : 'X', 'BKR' : 'X', 'CKM' : 'X', 'BDY' : 'X', 'RRS' : 'X', 'HTN' : 'X', 'GKG' : 'X', 'TRR' : 'X', 'AWH' : 'X', 'NMR' : 'X', 
'BMG' : 'X', 'DTB' : 'X', 'HRY' : 'X', 'HWM' : 'X', 'KDR' : 'X', 'HAW' : 'X', 'DTS' : 'X', 'TAC' : 'Y', 'VDC' : 'X', 'HBT' : 'X', 
'WGA' : 'X', 'GYN' : 'X', 'KGN' : 'X', 'NNT' : 'X', 'YDW' : 'X', 'BRB' : 'X', 'KKW' : 'X', 'AYS' : 'X', 'WWR' : 'X', 'CGR' : 'R', 
'VTW' : 'X', 'MWR' : 'X', 'MSS' : 'X', 'ANA' : 'X', 'TTH' : 'X', 'CAS' : 'X', 'AYC' : 'X', 'BYS' : 'X', 'WNN' : 'X', 'DKM' : 'X', 
'SAM' : 'X', 'TYK' : 'X', 'YDH' : 'X', 'RSD' : 'X', 'ADT' : 'X', 'BBA' : 'X', 'MDG' : 'X', 'KKN' : 'X', 'STN' : 'X', 'CWH' : 'X', 
'YYN' : 'X', 'MDT' : 'X', 'SNA' : 'X', 'MRC' : 'X', 'CAV' : 'X', 'CWR' : 'X', 'GBB' : 'X', 'KKG' : 'X', 'GBT' : 'X', 'GSG' : 'X', 
'SDM' : 'X', 'AHY' : 'X', 'THY' : 'X', 'HTH' : 'X', 'VRA' : 'X', 'MTR' : 'X', 'HMT' : 'X', 'WBM' : 'X', 'VDK' : 'X', 'VBT' : 'X', 
'KGK' : 'X', 'WSW' : 'X', 'ACV' : 'T', 'YNR' : 'X', 'MAH' : 'X', 'CMB' : 'X', 'GMV' : 'X', 'GHW' : 'X', 'HAD' : 'X', 'BTG' : 'X', 
'TDN' : 'X', 'KRN' : 'X', 'WMH' : 'X', 'KMB' : 'X', 'ACN' : 'T', 'GGC' : 'G', 'YKW' : 'X', 'RVK' : 'X', 'NBM' : 'X', 'VWN' : 'X', 
'TNC' : 'X', 'KRY' : 'X', 'TSB' : 'X', 'GWW' : 'X', 'THC' : 'X', 'TAD' : 'X', 'YRH' : 'X', 'TCK' : 'S', 'MDN' : 'X', 'GRD' : 'X', 
'YDM' : 'X', 'YSN' : 'X', 'CRN' : 'X', 'RGB' : 'X', 'YBW' : 'X', 'WYD' : 'X', 'GGK' : 'G', 'GMH' : 'X', 'CRW' : 'X', 'YHS' : 'X', 
'BBD' : 'X', 'AVD' : 'X', 'BSC' : 'X', 'NYH' : 'X', 'GMB' : 'X', 'RTV' : 'X', 'HDK' : 'X', 'WBC' : 'X', 'VTK' : 'X', 'MTC' : 'X', 
'AYK' : 'X', 'BBV' : 'X', 'DKH' : 'X', 'KSC' : 'X', 'DDB' : 'X', 'HHH' : 'X', 'CNW' : 'X', 'KBH' : 'X', 'NVR' : 'X', 'YTA' : 'L', 
'DDV' : 'X', 'VWB' : 'X', 'TVN' : 'X', 'SCA' : 'X', 'AHC' : 'X', 'MDW' : 'X', 'AGT' : 'S', 'RMM' : 'X', 'MTN' : 'X', 'WMN' : 'X', 
'TYW' : 'X', 'RTB' : 'X', 'CBT' : 'X', 'NSK' : 'X', 'VRB' : 'X', 'TGW' : 'X', 'BRW' : 'X', 'VTC' : 'X', 'NMV' : 'X', 'WHK' : 'X', 
'BRT' : 'X', 'NTS' : 'X', 'NWW' : 'X', 'RHD' : 'X', 'TTR' : 'L', 'VRW' : 'X', 'YDT' : 'X', 'VWV' : 'X', 'WCD' : 'X', 'MCC' : 'X', 
'TDS' : 'X', 'KTB' : 'X', 'THB' : 'X', 'WKS' : 'X', 'KMT' : 'X', 'NTK' : 'X', 'KHC' : 'X', 'DDD' : 'X', 'CVC' : 'X', 'ATV' : 'X', 
'BMW' : 'X', 'CYY' : 'X', 'MVV' : 'X', 'MDV' : 'X', 'SMR' : 'X', 'SHR' : 'X', 'NHY' : 'X', 'SVY' : 'X', 'KNG' : 'X', 'SHH' : 'X', 
'SHB' : 'X', 'SNW' : 'X', 'BCH' : 'X', 'YDC' : 'X', 'KMA' : 'X', 'HNS' : 'X', 'MNY' : 'X', 'TKS' : 'X', 'YWK' : 'X', 'TCV' : 'S', 
'MYY' : 'X', 'KRW' : 'X', 'YDS' : 'X', 'KBW' : 'X', 'NBN' : 'X', 'DYB' : 'X', 'CWS' : 'X', 'CWD' : 'X', 'AKV' : 'X', 'THD' : 'X', 
'AAN' : 'X', 'WRH' : 'X', 'RCS' : 'X', 'DSV' : 'X', 'NYD' : 'X', 'BGT' : 'X', 'DYH' : 'X', 'BDH' : 'X', 'KTV' : 'X', 'KWB' : 'X', 
'KMV' : 'X', 'DSB' : 'X', 'SHY' : 'X', 'VRS' : 'X', 'KHN' : 'X', 'MNN' : 'X', 'CGC' : 'R', 'ATG' : 'M', 'CMT' : 'X', 'KHT' : 'X', 
'AYB' : 'X', 'CDW' : 'X', 'SDB' : 'X', 'TWM' : 'X', 'VHN' : 'X', 'KSV' : 'X', 'VBN' : 'X', 'ATS' : 'X', 'THN' : 'X', 'NDH' : 'X', 
'KWS' : 'X', 'GSH' : 'X', 'NNH' : 'X', 'KWM' : 'X', 'CGN' : 'R', 'DYC' : 'X', 'VVT' : 'X', 'BRA' : 'X', 'AVS' : 'X', 'GCH' : 'A', 
'HHT' : 'X', 'HYY' : 'X', 'RAA' : 'X', 'DRG' : 'X', 'DAM' : 'X', 'CGD' : 'R', 'RCK' : 'X', 'WGM' : 'X', 'RYV' : 'X', 'BBG' : 'X', 
'KSK' : 'X', 'SHW' : 'X', 'MYR' : 'X', 'TYC' : 'X', 'MGB' : 'X', 'HGA' : 'X', 'HNB' : 'X', 'YDR' : 'X', 'DVK' : 'X', 'KCM' : 'X', 
'VHT' : 'X', 'SMT' : 'X', 'MMR' : 'X', 'NCW' : 'X', 'WRN' : 'X', 'WMR' : 'X', 'BVT' : 'X', 'CVS' : 'X', 'GGB' : 'G', 'MRA' : 'X', 
'KYR' : 'X', 'BNM' : 'X', 'TKM' : 'X', 'NVM' : 'X', 'ACY' : 'T', 'VBD' : 'X', 'RVC' : 'X', 'RCH' : 'X', 'CVV' : 'X', 'BNG' : 'X', 
'HSD' : 'X', 'DMD' : 'X', 'GSC' : 'X', 'CHN' : 'X', 'YTN' : 'X', 'CCH' : 'P', 'AGD' : 'X', 'DST' : 'X', 'RKR' : 'X', 'DKS' : 'X', 
'GGS' : 'G', 'MYG' : 'X', 'HGB' : 'X', 'WAG' : 'X', 'VGK' : 'X', 'NVN' : 'X', 'WNT' : 'X', 'KCG' : 'X', 'BGY' : 'X', 'GGA' : 'G', 
'DNT' : 'X', 'RGM' : 'X', 'TTM' : 'X', 'RMA' : 'X', 'AGR' : 'R', 'CTN' : 'L', 'GVN' : 'X', 'HNM' : 'X', 'NCY' : 'X', 'VAV' : 'X', 
'NWK' : 'X', 'TBN' : 'X', 'CMH' : 'X', 'GBG' : 'X', 'NGS' : 'X', 'WSM' : 'X', 'VTG' : 'X', 'NCT' : 'X', 'GHH' : 'X', 'CWM' : 'X', 
'YDB' : 'X', 'HVB' : 'X', 'GVK' : 'X', 'NVB' : 'X', 'GHK' : 'X', 'RYK' : 'X', 'KDS' : 'X', 'DDA' : 'X', 'VNN' : 'X', 'TBB' : 'X', 
'HNH' : 'X', 'NGB' : 'X', 'RAK' : 'X', 'KYT' : 'X', 'NBS' : 'X', 'GTN' : 'V', 'VVG' : 'X', 'MMM' : 'X', 'AGV' : 'X', 'DMS' : 'X', 
'MTH' : 'X', 'BBC' : 'X', 'SHN' : 'X', 'MHV' : 'X', 'HBV' : 'X', 'TTK' : 'X', 'HKT' : 'X', 'NBC' : 'X', 'DYK' : 'X', 'SSY' : 'X', 
'RDH' : 'X', 'DSD' : 'X', 'VMG' : 'X', 'KMM' : 'X', 'AKC' : 'X', 'KHH' : 'X', 'DYV' : 'X', 'GDA' : 'X', 'NYB' : 'X', 'HCD' : 'X', 
'TCW' : 'S', 'GSK' : 'X', 'SMC' : 'X', 'HWB' : 'X', 'KAT' : 'X', 'VDT' : 'X', 'TBC' : 'X', 'TWV' : 'X', 'KBS' : 'X', 'CBS' : 'X', 
'HVW' : 'X', 'STT' : 'X', 'THH' : 'X', 'SAR' : 'X', 'YTS' : 'X', 'SDS' : 'X', 'AHV' : 'X', 'BHB' : 'X', 'ABR' : 'X', 'AKB' : 'X', 
'VKV' : 'X', 'GKN' : 'X', 'VYS' : 'X', 'NMW' : 'X', 'CKW' : 'X', 'WRW' : 'X', 'GMR' : 'X', 'RGT' : 'X', 'KAY' : 'X', 'YKT' : 'X', 
'MDD' : 'X', 'YHW' : 'X', 'ARW' : 'X', 'ACD' : 'T', 'HKA' : 'X', 'KYY' : 'X', 'KKH' : 'X', 'AHD' : 'X', 'YRR' : 'X', 'NCD' : 'X', 
'GDK' : 'X', 'GWK' : 'X', 'RMW' : 'X', 'KWG' : 'X', 'VVA' : 'X', 'TAB' : 'X', 'HVV' : 'X', 'KMC' : 'X', 'HGG' : 'X', 'VYA' : 'X', 
'RHB' : 'X', 'MAR' : 'X', 'HDY' : 'X', 'GSA' : 'X', 'KVD' : 'X', 'BWC' : 'X', 'BNB' : 'X', 'MWD' : 'X', 'WWH' : 'X', 'TGY' : 'C', 
'YTD' : 'X', 'CGB' : 'R', 'KCN' : 'X', 'KDV' : 'X', 'BWA' : 'X', 'NDD' : 'X', 'ACK' : 'T', 'WCV' : 'X', 'CBG' : 'X', 'WYK' : 'X', 
'MYT' : 'X', 'SDV' : 'X', 'SDH' : 'X', 'HAM' : 'X', 'SGY' : 'X', 'SAH' : 'X', 'KBK' : 'X', 'RGY' : 'X', 'AAY' : 'N', 'TMY' : 'X', 
'MCG' : 'X', 'RGA' : 'X', 'ANS' : 'X', 'NTM' : 'X', 'YAN' : 'X', 'HHK' : 'X', 'VSK' : 'X', 'CMA' : 'X', 'MAW' : 'X', 'CVD' : 'X', 
'TKR' : 'X', 'CDB' : 'X', 'YYD' : 'X', 'HGH' : 'X', 'CAK' : 'X', 'KSD' : 'X', 'VGG' : 'X', 'VWH' : 'X', 'HAC' : 'X', 'RKK' : 'X', 
'DBW' : 'X', 'GCS' : 'A', 'RHA' : 'X', 'YRM' : 'X', 'VYT' : 'X', 'VGS' : 'X', 'MAT' : 'X', 'HCA' : 'X', 'NTY' : 'X', 'WCM' : 'X', 
'GAH' : 'X', 'GYR' : 'X', 'BAG' : 'X', 'SSS' : 'X', 'BDV' : 'X', 'YYT' : 'X', 'TAK' : 'X', 'GYW' : 'X', 'BDK' : 'X', 'KRR' : 'X', 
'VCN' : 'X', 'CVM' : 'X', 'MBC' : 'X', 'CBW' : 'X', 'ADV' : 'X', 'VAW' : 'X', 'WVH' : 'X', 'KGM' : 'X', 'VRH' : 'X', 'ADH' : 'X', 
'YWB' : 'X', 'CDY' : 'X', 'VND' : 'X', 'SBT' : 'X', 'KDD' : 'X', 'NYG' : 'X', 'MHT' : 'X', 'KRM' : 'X', 'KHD' : 'X', 'CCM' : 'P', 
'KHM' : 'X', 'VAM' : 'X', 'HWC' : 'X', 'RAN' : 'X', 'CNA' : 'X', 'YMH' : 'X', 'HDW' : 'X', 'CMV' : 'X', 'CMY' : 'X', 'RRD' : 'X', 
'KYW' : 'X', 'TRH' : 'X', 'ARS' : 'X', 'YST' : 'X', 'BVS' : 'X', 'SBK' : 'X', 'RGH' : 'X', 'SVM' : 'X', 'BAY' : 'X', 'KRD' : 'X', 
'RDG' : 'X', 'SWG' : 'X', 'CRC' : 'X', 'YHT' : 'X', 'MVS' : 'X', 'SVK' : 'X', 'AYA' : 'X', 'YMN' : 'X', 'RDV' : 'X', 'DCH' : 'X', 
'CGV' : 'R', 'GRK' : 'X', 'YGT' : 'X', 'WSK' : 'X', 'DNK' : 'X', 'GBN' : 'X', 'AMW' : 'X', 'NYS' : 'X', 'HMS' : 'X', 'CSK' : 'X', 
'HMY' : 'X', 'AKA' : 'X', 'NMS' : 'X', 'HVS' : 'X', 'HSV' : 'X', 'SYT' : 'X', 'MCA' : 'X', 'GHB' : 'X', 'SAB' : 'X', 'GSB' : 'X', 
'VRV' : 'X', 'YNW' : 'X', 'ASK' : 'X', 'GRT' : 'X', 'CMG' : 'X', 'WKD' : 'X', 'VDH' : 'X', 'RNV' : 'X', 'TWC' : 'X', 'CNM' : 'X', 
'DVN' : 'X', 'HWA' : 'X', 'TMS' : 'X', 'NMA' : 'X', 'RKB' : 'X', 'CKR' : 'X', 'CSC' : 'X', 'AYH' : 'X', 'SWA' : 'X', 'KTR' : 'X', 
'YBT' : 'X', 'RNB' : 'X', 'SCC' : 'X', 'GMM' : 'X', 'SWH' : 'X', 'WNR' : 'X', 'YGA' : 'X', 'KVB' : 'X', 'NBT' : 'X', 'BNA' : 'X', 
'DBV' : 'X', 'BNR' : 'X', 'GTD' : 'V', 'SBS' : 'X', 'KTC' : 'X', 'WKT' : 'X', 'KSB' : 'X', 'SRW' : 'X', 'ARK' : 'X', 'CMD' : 'X', 
'BBN' : 'X', 'VMD' : 'X', 'NKK' : 'X', 'VCY' : 'X', 'TKV' : 'X', 'ABH' : 'X', 'SBD' : 'X', 'WBH' : 'X', 'YKC' : 'X', 'HNG' : 'X', 
'BWT' : 'X', 'KRC' : 'X', 'TAY' : 'Y', 'BHK' : 'X', 'TYY' : 'X', 'TVC' : 'X', 'WSH' : 'X', 'NDG' : 'X', 'RRG' : 'X', 'YCD' : 'X', 
'HHG' : 'X', 'MSW' : 'X', 'SRV' : 'X', 'RVD' : 'X', 'VMY' : 'X', 'SHM' : 'X', 'VVR' : 'X', 'RCW' : 'X', 'KBV' : 'X', 'MSR' : 'X', 
'NKT' : 'X', 'YTW' : 'X', 'NMD' : 'X', 'RNR' : 'X', 'MBN' : 'X', 'CSW' : 'X', 'YWV' : 'X', 'MGT' : 'X', 'ASY' : 'X', 'HRM' : 'X', 
'DDN' : 'X', 'DRS' : 'X', 'DTD' : 'X', 'WHW' : 'X', 'WRG' : 'X', 'ACB' : 'T', 'WYH' : 'X', 'DYS' : 'X', 'DHA' : 'X', 'NVD' : 'X', 
'NAW' : 'X', 'BYV' : 'X', 'HGM' : 'X', 'RVS' : 'X', 'TCB' : 'S', 'NAK' : 'X', 'KNB' : 'X', 'BAV' : 'X', 'KVK' : 'X', 'TCD' : 'S', 
'YDN' : 'X', 'WYY' : 'X', 'NAC' : 'X', 'HSH' : 'X', 'VSN' : 'X', 'HSW' : 'X', 'VWM' : 'X', 'RWN' : 'X', 'DGK' : 'X', 'YRC' : 'X', 
'CMW' : 'X', 'SKR' : 'X', 'RDS' : 'X', 'MWT' : 'X', 'VGC' : 'X', 'VMT' : 'X', 'RRW' : 'X', 'MGR' : 'R', 'THT' : 'X', 'DWN' : 'X', 
'MDH' : 'X', 'VYB' : 'X', 'NCN' : 'X', 'KGG' : 'X', 'KVG' : 'X', 'MVT' : 'X', 'AWR' : 'X', 'RMD' : 'X', 'GVR' : 'X', 'CRS' : 'X', 
'YGK' : 'X', 'CHG' : 'X', 'CTR' : 'L', 'VAN' : 'X', 'TKG' : 'X', 'BMB' : 'X', 'DVC' : 'X', 'VYY' : 'X', 'NAD' : 'X', 'RWY' : 'X', 
'YWY' : 'X', 'SAD' : 'X', 'NVY' : 'X', 'MTG' : 'X', 'KTD' : 'X', 'BKV' : 'X', 'SWB' : 'X', 'CKC' : 'X', 'GDG' : 'X', 'BKB' : 'X', 
'WTD' : 'X', 'AYG' : 'X', 'NRY' : 'X', 'DBM' : 'X', 'AWD' : 'X', 'GMY' : 'X', 'NAM' : 'X', 'YCT' : 'X', 'SMV' : 'X', 'RRT' : 'X', 
'VCA' : 'X', 'ANK' : 'X', 'YNN' : 'X', 'KCR' : 'X', 'YYG' : 'X', 'DMA' : 'X', 'VNA' : 'X', 'TAM' : 'X', 'GNY' : 'X', 'HNT' : 'X', 
'WTG' : 'X', 'CAT' : 'H', 'DBB' : 'X', 'VMV' : 'X', 'SVD' : 'X', 'CDV' : 'X', 'MNA' : 'X', 'BTR' : 'X', 'AST' : 'X', 'VWY' : 'X', 
'DGG' : 'X', 'YDG' : 'X', 'DMM' : 'X', 'TGA' : None, 'HYR' : 'X', 'TGN' : 'X', 'KSY' : 'X', 'NHN' : 'X', 'GTV' : 'V', 'NSR' : 'X', 
'TDB' : 'X', 'MNH' : 'X', 'VWK' : 'X', 'ATC' : 'I', 'HDM' : 'X', 'TAS' : 'X', 'DWA' : 'X', 'ARR' : 'X', 'NTA' : 'X', 'SSB' : 'X', 
'YBS' : 'X', 'MTM' : 'X', 'MSG' : 'X', 'MBD' : 'X', 'YRW' : 'X', 'YSS' : 'X', 'WTW' : 'X', 'VDN' : 'X', 'HDG' : 'X', 'HWR' : 'X', 
'YVS' : 'X', 'DWH' : 'X', 'DRM' : 'X', 'NGN' : 'X', 'HBR' : 'X', 'RHY' : 'X', 'YHA' : 'X', 'CDA' : 'X', 'AGG' : 'R', 'HGK' : 'X', 
'AMT' : 'X', 'RYY' : 'X', 'VNK' : 'X', 'YDY' : 'X', 'YYW' : 'X', 'YGB' : 'X', 'MYK' : 'X', 'KAW' : 'X', 'TDW' : 'X', 'MMG' : 'X', 
'TBV' : 'X', 'MBV' : 'X', 'ANN' : 'X', 'VYD' : 'X', 'BWB' : 'X', 'SSV' : 'X', 'MDB' : 'X', 'MGV' : 'X', 'WDC' : 'X', 'VMH' : 'X', 
'NVS' : 'X', 'HMA' : 'X', 'VCC' : 'X', 'DDG' : 'X', 'NSA' : 'X', 'WBA' : 'X', 'RSS' : 'X', 'DVY' : 'X', 'KHK' : 'X', 'GVA' : 'X', 
'MGG' : 'R', 'VGH' : 'X', 'KTH' : 'X', 'CSG' : 'X', 'HAS' : 'X', 'AVB' : 'X', 'YGS' : 'X', 'GBY' : 'X', 'WSG' : 'X', 'AKD' : 'X', 
'ATY' : 'I', 'WDR' : 'X', 'KSH' : 'X', 'RSM' : 'X', 'YKK' : 'X', 'NRS' : 'X', 'WSD' : 'X', 'WHN' : 'X', 'ARB' : 'X', 'HHB' : 'X', 
'VRM' : 'X', 'ASR' : 'X', 'CYV' : 'X', 'AKK' : 'X', 'DMW' : 'X', 'BTW' : 'X', 'DGR' : 'X', 'RBH' : 'X', 'ABM' : 'X', 'YVR' : 'X', 
'DGD' : 'X', 'MNS' : 'X', 'ARD' : 'X', 'CND' : 'X', 'SSM' : 'X', 'BNW' : 'X', 'HAV' : 'X', 'SYH' : 'X', 'BHM' : 'X', 'RRM' : 'X', 
'WAV' : 'X', 'KTG' : 'X', 'YTB' : 'X', 'DNM' : 'X', 'BSH' : 'X', 'WRR' : 'X', 'KWN' : 'X', 'AMN' : 'X', 'STR' : 'X', 'SKG' : 'X', 
'KVN' : 'X', 'ASW' : 'X', 'MYN' : 'X', 'BKG' : 'X', 'GBH' : 'X', 'VYK' : 'X', 'WBV' : 'X', 'KGB' : 'X', 'HGW' : 'X', 'MTS' : 'X', 
'TVD' : 'X', 'TCY' : 'S', 'KVA' : 'X', 'VWS' : 'X', 'NNY' : 'X', 'GRH' : 'X', 'BYH' : 'X', 'TYN' : 'X', 'WHD' : 'X', 'DSN' : 'X', 
'SCK' : 'X', 'VGM' : 'X', 'GNA' : 'X', 'WTM' : 'X', 'ASG' : 'X', 'VKM' : 'X', 'DTY' : 'X', 'ANC' : 'X', 'BWD' : 'X', 'YSD' : 'X', 
'SYR' : 'X', 'CHH' : 'X', 'KHY' : 'X', 'CYK' : 'X', 'HHR' : 'X', 'RTM' : 'X', 'CSY' : 'X', 'RAR' : 'X', 'SDR' : 'X', 'GST' : 'X', 
'VSM' : 'X', 'VRN' : 'X', 'HBS' : 'X', 'TKA' : 'X', 'CGG' : 'R', 'BWM' : 'X', 'BCM' : 'X', 'NCG' : 'X', 'RMH' : 'X', 'GAG' : 'E', 
'YMK' : 'X', 'WWK' : 'X', 'CVH' : 'X', 'YGH' : 'X', 'BMM' : 'X', 'SWW' : 'X', 'CGY' : 'R', 'RMR' : 'X', 'GYA' : 'X', 'HGN' : 'X', 
'BKA' : 'X', 'YMD' : 'X', 'AWV' : 'X', 'SMN' : 'X', 'YVK' : 'X', 'VTT' : 'X', 'BNY' : 'X', 'CKV' : 'X', 'DNV' : 'X', 'AGK' : 'X', 
'MWK' : 'X', 'KNW' : 'X', 'DSG' : 'X', 'HAN' : 'X', 'SMH' : 'X', 'KSM' : 'X', 'WMK' : 'X', 'HWY' : 'X', 'YNM' : 'X', 'HMH' : 'X', 
'YNG' : 'X', 'CNG' : 'X', 'MMD' : 'X', 'GDT' : 'X', 'MHB' : 'X', 'AKT' : 'X', 'WAT' : 'X', 'CCY' : 'P', 'YWD' : 'X', 'KMW' : 'X', 
'STK' : 'X', 'ARH' : 'X', 'GRB' : 'X', 'GBS' : 'X', 'DKB' : 'X', 'NYT' : 'X', 'SVW' : 'X', 'DAG' : 'X', 'MHH' : 'X', 'VDY' : 'X', 
'RVV' : 'X', 'NKN' : 'X', 'TCN' : 'S', 'GNG' : 'X', 'TBS' : 'X', 'YHV' : 'X', 'GNK' : 'X', 'BRN' : 'X', 'BCT' : 'X', 'RBR' : 'X', 
'WHT' : 'X', 'YMR' : 'X', 'RAH' : 'X', 'NWD' : 'X', 'NRK' : 'X', 'YCC' : 'X', 'HDV' : 'X', 'AAB' : 'X', 'DCW' : 'X', 'TNB' : 'X', 
'RWT' : 'X', 'WKH' : 'X', 'HCM' : 'X', 'DYN' : 'X', 'BCB' : 'X', 'NAN' : 'X', 'ART' : 'X', 'VVY' : 'X', 'YRS' : 'X', 'TDK' : 'X', 
'DMY' : 'X', 'SAG' : 'X', 'VKT' : 'X', 'SVG' : 'X', 'BGH' : 'X', 'TTD' : 'X', 'HNW' : 'X', 'WDB' : 'X', 'ADB' : 'X', 'GCG' : 'A', 
'HBA' : 'X', 'MTW' : 'X', 'CMR' : 'X', 'HHS' : 'X', 'KWV' : 'X', 'RVM' : 'X', 'WKC' : 'X', 'RRB' : 'X', 'GWD' : 'X', 'MAB' : 'X', 
'DKN' : 'X', 'MKC' : 'X', 'VCV' : 'X', 'NAG' : 'X', 'YWA' : 'X', 'MCW' : 'X', 'HYW' : 'X', 'YBV' : 'X', 'WVN' : 'X', 'ACM' : 'T', 
'YDD' : 'X', 'STC' : 'X', 'RGG' : 'X', 'GYC' : 'X', 'MWW' : 'X', 'GCV' : 'A', 'RRY' : 'X', 'AWS' : 'X', 'SKT' : 'X', 'TST' : 'X', 
'WWD' : 'X', 'VDB' : 'X', 'SAK' : 'X', 'RCC' : 'X', 'BCG' : 'X', 'NYR' : 'X', 'DWR' : 'X', 'DNY' : 'X', 'VBR' : 'X', 'RWK' : 'X', 
'AMR' : 'X', 'YSA' : 'X', 'VYM' : 'X', 'GAV' : 'X', 'GWN' : 'X', 'SVT' : 'X', 'KSW' : 'X', 'BHT' : 'X', 'CBK' : 'X', 'BSG' : 'X', 
'DYT' : 'X', 'GCN' : 'A', 'BNC' : 'X', 'NSG' : 'X', 'AKH' : 'X', 'VAB' : 'X', 'RTK' : 'X', 'SDW' : 'X', 'YNV' : 'X', 'MSK' : 'X', 
'NKV' : 'X', 'NHS' : 'X', 'TTN' : 'X', 'HMG' : 'X', 'DAN' : 'X', 'AYW' : 'X', 'GCA' : 'A', 'MKA' : 'X', 'NDY' : 'X', 'MSV' : 'X', 
'TMH' : 'X', 'GKA' : 'X', 'DMT' : 'X', 'RYA' : 'X', 'AWC' : 'X', 'RCT' : 'X', 'DTT' : 'X', 'CNK' : 'X', 'GDM' : 'X', 'CTA' : 'L', 
'YKB' : 'X', 'NDS' : 'X', 'RKD' : 'X', 'HRG' : 'X', 'RSK' : 'X', 'CKH' : 'X', 'VWT' : 'X', 'YVD' : 'X', 'MAD' : 'X', 'KGA' : 'X', 
'NTV' : 'X', 'SHD' : 'X', 'CBB' : 'X', 'RGV' : 'X', 'YWR' : 'X', 'SVR' : 'X', 'SRM' : 'X', 'MMW' : 'X', 'KSA' : 'X', 'HKC' : 'X', 
'NBG' : 'X', 'CBD' : 'X', 'RSY' : 'X', 'DDW' : 'X', 'RKN' : 'X', 'BGD' : 'X', 'RGC' : 'X', 'YDA' : 'X', 'BWS' : 'X', 'STM' : 'X', 
'TYM' : 'X', 'YVM' : 'X', 'GGY' : 'G', 'DKT' : 'X', 'NKC' : 'X', 'TAT' : 'Y', 'RHN' : 'X', 'YHM' : 'X', 'SGN' : 'X', 'MDS' : 'X', 
'DAT' : 'X', 'YND' : 'X', 'SVS' : 'X', 'WCK' : 'X', 'DSA' : 'X', 'TBH' : 'X', 'SWT' : 'X', 'KHV' : 'X', 'YTH' : 'X', 'ATN' : 'X', 
'GHY' : 'X', 'YKH' : 'X', 'CHV' : 'X', 'MCB' : 'X', 'VHG' : 'X', 'THS' : 'X', 'CWT' : 'X', 'NBA' : 'X', 'MRH' : 'X', 'GAB' : 'X', 
'NHK' : 'X', 'HAB' : 'X', 'NKR' : 'X', 'BGK' : 'X', 'YSV' : 'X', 'HVA' : 'X', 'GHN' : 'X', 'DGT' : 'X', 'DGB' : 'X', 'KNS' : 'X', 
'WAR' : 'X', 'BSW' : 'X', 'CST' : 'X', 'HRA' : 'X', 'DBG' : 'X', 'BRS' : 'X', 'DAB' : 'X', 'ABB' : 'X', 'CBA' : 'X', 'KYC' : 'X', 
'GGD' : 'G', 'RYM' : 'X', 'CHC' : 'X', 'RTH' : 'X', 'CKD' : 'X', 'TGC' : 'C', 'WWW' : 'X', 'YHD' : 'X', 'WNW' : 'X', 'SDA' : 'X', 
'DDK' : 'X', 'HKY' : 'X', 'KCD' : 'X', 'SRY' : 'X', 'NDV' : 'X', 'DAR' : 'X', 'YYR' : 'X', 'HTM' : 'X', 'AMC' : 'X', 'VBA' : 'X', 
'NVW' : 'X', 'TRV' : 'X', 'SGD' : 'X', 'NDN' : 'X', 'KGR' : 'X', 'BAT' : 'X', 'BRH' : 'X', 'YTK' : 'X', 'ASC' : 'X', 'YAR' : 'X', 
'NGY' : 'X', 'WVS' : 'X', 'TKN' : 'X', 'MKB' : 'X', 'TGH' : 'X', 'ACW' : 'T', 'YRV' : 'X', 'KWC' : 'X', 'BYT' : 'X', 'GSY' : 'X', 
'RCA' : 'X', 'VMC' : 'X', 'TYV' : 'X', 'MMH' : 'X', 'ARM' : 'X', 'BSY' : 'X', 'MRV' : 'X', 'CGK' : 'R', 'DND' : 'X', 'WMB' : 'X', 
'YKV' : 'X', 'GWY' : 'X', 'SAW' : 'X', 'MMC' : 'X', 'DGC' : 'X', 'TBD' : 'X', 'WTK' : 'X', 'CNB' : 'X', 'BTM' : 'X', 'DHN' : 'X', 
'BHW' : 'X', 'DSC' : 'X', 'NHB' : 'X', 'VNG' : 'X', 'WKG' : 'X', 'RNY' : 'X', 'KWT' : 'X', 'WVT' : 'X', 'NWG' : 'X', 'AYV' : 'X', 
'RDN' : 'X', 'GDV' : 'X', 'CYN' : 'X', 'SWK' : 'X', 'ASD' : 'X', 'RDT' : 'X', 'RHK' : 'X', 'DWB' : 'X', 'SRS' : 'X', 'KDT' : 'X', 
'NRW' : 'X', 'VVN' : 'X', 'TCG' : 'S', 'KHG' : 'X', 'MVB' : 'X', 'AYM' : 'X', 'NMT' : 'X', 'WDH' : 'X', 'TDV' : 'X', 'BVN' : 'X', 
'NGV' : 'X', 'ACH' : 'T', 'BRK' : 'X', 'MCY' : 'X', 'VMW' : 'X', 'CYB' : 'X', 'MHM' : 'X', 'KCV' : 'X', 'GWA' : 'X', 'DWT' : 'X', 
'STW' : 'X', 'NCR' : 'X', 'CGM' : 'R', 'MWV' : 'X', 'MVK' : 'X', 'GKV' : 'X', 'VAD' : 'X', 'DYA' : 'X', 'GWT' : 'X', 'BDB' : 'X', 
'KDH' : 'X', 'SGV' : 'X', 'GSV' : 'X', 'BYG' : 'X', 'TWY' : 'X', 'SMW' : 'X', 'YWW' : 'X', 'MNK' : 'X', 'CVT' : 'X', 'NWV' : 'X', 
'TMW' : 'X', 'TBM' : 'X', 'DDS' : 'X', 'RYT' : 'X', 'TCA' : 'S', 'KDN' : 'X', 'MTA' : 'X', 'BCC' : 'X', 'GAN' : 'X', 'TTG' : 'L', 
'ADA' : 'X', 'CSN' : 'X', 'MKD' : 'X', 'MHK' : 'X', 'STG' : 'X', 'RSA' : 'X', 'CDT' : 'X', 'CDK' : 'X', 'WWT' : 'X', 'TBT' : 'X', 
'GVS' : 'X', 'NCH' : 'X', 'MKT' : 'X', 'KMR' : 'X', 'NDB' : 'X', 'MWS' : 'X', 'MMS' : 'X', 'DCB' : 'X', 'WMW' : 'X', 'SBH' : 'X', 
'CAN' : 'X', 'BVH' : 'X', 'TMM' : 'X', 'HRW' : 'X', 'NSV' : 'X', 'KGC' : 'X', 'RTN' : 'X', 'KAC' : 'X', 'HNC' : 'X', 'DAY' : 'X', 
'KVR' : 'X', 'WTY' : 'X', 'VSY' : 'X', 'CSM' : 'X', 'WVV' : 'X', 'WYT' : 'X', 'GWC' : 'X', 'MHA' : 'X', 'WMV' : 'X', 'TGS' : 'X', 
'RHV' : 'X', 'NGH' : 'X', 'VVW' : 'X', 'HRV' : 'X', 'KGY' : 'X', 'BMN' : 'X', 'KWH' : 'X', 'VCS' : 'X', 'NNV' : 'X', 'BVK' : 'X', 
'HND' : 'X', 'SYD' : 'X', 'YSW' : 'X', 'WKV' : 'X', 'STD' : 'X', 'RNN' : 'X', 'WHB' : 'X', 'HSR' : 'X', 'YMC' : 'X', 'DRD' : 'X', 
'TYT' : 'X', 'CAD' : 'X', 'VBW' : 'X', 'KTY' : 'X', 'MYS' : 'X', 'NAS' : 'X', 'GVH' : 'X', 'HRH' : 'X', 'CHW' : 'X', 'WYG' : 'X', 
'DRK' : 'X', 'DVV' : 'X', 'HSM' : 'X', 'SGC' : 'X', 'CAH' : 'X', 'MVN' : 'X', 'RVB' : 'X', 'HKD' : 'X', 'NWY' : 'X', 'NBK' : 'X', 
'WVK' : 'X', 'ANV' : 'X', 'HCH' : 'X', 'YMM' : 'X', 'KNM' : 'X', 'GRA' : 'X', 'MAM' : 'X', 'BMS' : 'X', 'CVB' : 'X', 'BVG' : 'X', 
'MMY' : 'X', 'RBB' : 'X', 'WHH' : 'X', 'HCN' : 'X', 'VKR' : 'X', 'RRN' : 'X', 'YKA' : 'X', 'RSG' : 'X', 'HBD' : 'X', 'KNK' : 'X', 
'GYT' : 'X', 'CGA' : 'R', 'WGV' : 'X', 'SMM' : 'X', 'AGS' : 'X', 'VNS' : 'X', 'TSA' : 'X', 'DVT' : 'X', 'YWH' : 'X', 'DKG' : 'X', 
'CDR' : 'X', 'AVG' : 'X', 'WMG' : 'X', 'HYM' : 'X', 'MSN' : 'X', 'TGK' : 'X', 'DBC' : 'X', 'WHS' : 'X', 'SHA' : 'X', 'KVY' : 'X', 
'RSN' : 'X', 'DRN' : 'X', 'GDD' : 'X', 'DVH' : 'X', 'DSW' : 'X', 'NBB' : 'X', 'YYY' : 'X', 'VMK' : 'X', 'HAH' : 'X', 'HTK' : 'X', 
'GYH' : 'X', 'YSY' : 'X', 'RST' : 'X', 'MSM' : 'X', 'VSG' : 'X', 'WAN' : 'X', 'SGS' : 'X', 'DAW' : 'X', 'NCS' : 'X', 'SCV' : 'X', 
'BNK' : 'X', 'SSC' : 'X', 'HVD' : 'X', 'KRK' : 'X', 'TAH' : 'X', 'MKS' : 'X', 'TGD' : 'X', 'SVN' : 'X', 'SWS' : 'X', 'VHM' : 'X', 
'CCB' : 'P', 'YVC' : 'X', 'DHH' : 'X', 'KAB' : 'X', 'TTY' : 'F', 'HTT' : 'X', 'NHW' : 'X', 'SWD' : 'X', 'YRG' : 'X', 'VAT' : 'X', 
'RAG' : 'X', 'YRN' : 'X', 'YVB' : 'X', 'KBY' : 'X', 'BNH' : 'X', 'HMD' : 'X', 'YCB' : 'X', 'CAM' : 'X', 'MYA' : 'X', 'GNT' : 'X', 
'BKC' : 'X', 'ANH' : 'X', 'WSR' : 'X', 'AVH' : 'X', 'AVV' : 'X', 'BSB' : 'X', 'WTR' : 'X', 'WVG' : 'X', 'KWA' : 'X', 'WTA' : 'X', 
'KMH' : 'X', 'KRH' : 'X', 'KMK' : 'X', 'HRT' : 'X', 'ATB' : 'X', 'CWV' : 'X', 'GHD' : 'X', 'HMN' : 'X', 'BGN' : 'X', 'VCB' : 'X', 
'NDT' : 'X', 'RWH' : 'X', 'BCN' : 'X', 'GHS' : 'X', 'KRV' : 'X', 'BKS' : 'X', 'MKY' : 'X', 'AVT' : 'X', 'RSV' : 'X', 'VNY' : 'X', 
'GTC' : 'V', 'GVT' : 'X', 'TKW' : 'X', 'YHK' : 'X', 'MBT' : 'X', 'RDY' : 'X', 'HAY' : 'X', 'YBA' : 'X', 'BYR' : 'X', 'VYR' : 'X', 
'KBT' : 'X', 'MKN' : 'X', 'CHB' : 'X', 'BCA' : 'X', 'GSW' : 'X', 'ASS' : 'X', 'MTV' : 'X', 'NMN' : 'X', 'GRS' : 'X', 'SVA' : 'X', 
'VGR' : 'X', 'RWV' : 'X', 'WBN' : 'X', 'KYG' : 'X', 'TMK' : 'X', 'ATD' : 'X', 'DVR' : 'X', 'KBN' : 'X', 'WYB' : 'X', 'VGY' : 'X', 
'NWH' : 'X', 'VSD' : 'X', 'HWT' : 'X', 'SYN' : 'X', 'VHK' : 'X', 'CCV' : 'P', 'WGY' : 'X', 'HCR' : 'X', 'BGG' : 'X', 'BHC' : 'X', 
'BND' : 'X', 'BVR' : 'X', 'AHH' : 'X', 'MNR' : 'X', 'MRB' : 'X', 'ASH' : 'X', 'NSH' : 'X', 'MBB' : 'X', 'VYC' : 'X', 'MHW' : 'X', 
'GRW' : 'X', 'CCR' : 'P', 'BKY' : 'X', 'ARN' : 'X', 'DRR' : 'X', 'GSR' : 'X', 'GAY' : 'D', 'KYK' : 'X', 'AHN' : 'X', 'NNC' : 'X', 
'CRR' : 'X', 'CSH' : 'X', 'BDR' : 'X', 'YYV' : 'X', 'AVM' : 'X', 'BTB' : 'X', 'SSW' : 'X', 'GAR' : 'E', 'TKH' : 'X', 'NAY' : 'X', 
'NRV' : 'X', 'YTC' : 'X', 'KGS' : 'X', 'THV' : 'X', 'NRC' : 'X', 'WCC' : 'X', 'TNA' : 'X', 'CTS' : 'L', 'GCB' : 'A', 'MCD' : 'X', 
'SHK' : 'X', 'VAC' : 'X', 'DTK' : 'X', 'WVD' : 'X', 'DKR' : 'X', 'NHG' : 'X', 'NNA' : 'X', 'GTW' : 'V', 'MKG' : 'X', 'DTA' : 'X', 
'RTR' : 'X', 'GHV' : 'X', 'CTY' : 'L', 'NKA' : 'X', 'KBD' : 'X', 'HHM' : 'X', 'WMM' : 'X', 'CVA' : 'X', 'VNB' : 'X', 'SHV' : 'X', 
'VRY' : 'X', 'ADW' : 'X', 'DYR' : 'X', 'BYA' : 'X', 'GGN' : 'G', 'DAH' : 'X', 'GGW' : 'G', 'MYW' : 'X', 'DRY' : 'X', 'RMG' : 'X', 
'RND' : 'X', 'TYH' : 'X', 'DHS' : 'X', 'TVT' : 'X', 'SHS' : 'X', 'VNW' : 'X', 'MSA' : 'X', 'KYB' : 'X', 'WSS' : 'X', 'MHN' : 'X', 
'CKB' : 'X', 'TWT' : 'X', 'WNS' : 'X', 'VVH' : 'X', 'SSA' : 'X', 'YCH' : 'X', 'MVA' : 'X', 'MMV' : 'X', 'RNG' : 'X', 'GYY' : 'X', 
'GSM' : 'X', 'ADR' : 'X', 'HTR' : 'X', 'MAS' : 'X', 'BMA' : 'X', 'NTT' : 'X', 'YSC' : 'X', 'ACT' : 'T', 'RDR' : 'X', 'DMC' : 'X', 
'NKG' : 'X', 'NHR' : 'X', 'VMB' : 'X', 'VVC' : 'X', 'RTA' : 'X', 'VNV' : 'X', 'GNH' : 'X', 'BSS' : 'X', 'KMN' : 'X', 'DWY' : 'X', 
'TBG' : 'X', 'VHA' : 'X', 'HSN' : 'X', 'VTB' : 'X', 'YWM' : 'X', 'ANG' : 'X', 'KRB' : 'X', 'WMD' : 'X', 'KYH' : 'X', 'DNC' : 'X', 
'BAH' : 'X', 'HWD' : 'X', 'RDW' : 'X', 'RAC' : 'X', 'NAT' : 'X', 'KSN' : 'X', 'VTS' : 'X', 'WHM' : 'X', 'HVG' : 'X', 'TTV' : 'X', 
'HGY' : 'X', 'VCH' : 'X', 'MSC' : 'X', 'RBC' : 'X', 'CCN' : 'P', 'TDR' : 'X', 'YCR' : 'X', 'YTG' : 'L', 'TWD' : 'X', 'CTD' : 'L', 
'HGS' : 'X', 'YMS' : 'X', 'HCW' : 'X', 'HVC' : 'X', 'GAD' : 'X'

        }

def complement(seqn):
    return seqn.translate(compl_transtable)

def translate(seqn):
    """
    Returns all the aminoacid sequences from a one nucleic sequence
    nucleic -> (start,end),(prot,cropednucl)
    """
    codons = ((p*3,seqn[p*3:p*3+3]) for p in range((len(seqn))//3))
    aminoacids = map(lambda c: (c[0],codontable[c[1]]),codons)
    # itertools.groupby splits with stop codons
    seplist = (list(c) for b, c in itertools.groupby(aminoacids, lambda c: bool(c[1])) if b)
    # merge petptides
    return(((p[0][0]+1,p[-1][0]+3),"".join([a[1] for a in p])) for p in seplist)

def getAllTranslations(seqn,minLength = None):
    def realCoordsAndCropNucl(seqptuple,frame,sense,seqn):
        (start,end),seqp = seqptuple
        return ((start+frame,end+frame) if sense == "+" else (len(seqn)+1-start-frame,len(seqn)+1-end-frame),(seqp,seqn[start+frame:end+frame]))
    seqn = seqn.upper()
    revseqn = "".join(reversed(complement(seqn)))
    for (sense, seq) in (("+",seqn),("-",revseqn)):
        for frame in range(3):
            seqp = tuple(filter(lambda s: len(s[1])>=minLength if minLength else True,translate(seq[frame:])))
            if seqp:
                yield (
                    (sense,frame+1),
                    map(lambda s: realCoordsAndCropNucl(s,frame,sense,seqn),seqp)
                       )

### TYPES

class BestHitsAnnotation(NamedTuple):
    qseqid: str
    LCA: list
    kw: str
    score: float
    evalue: list
    LCA_taxid: list
    subjects: list
    lengths: list
    idents: list

class BlastHit(NamedTuple):
    qseqid: str
    sseqid: str
    pident: float
    length: int
    mismatch: int
    gapopen: int
    qstart: int
    qend: int
    sstart: int
    send: int
    evalue: float
    bitscore: float


#"qseqid","score","evalue","lca_lineage","subject","length", "ident" #nov18

def cbh(path):
    return (CbhEntry(line.strip().split(";")) for line in itertools.islice(open(path),1,None))

BestHitFields = ()
