#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import glob,re,argparse,os,sqlite3

from taxadb.accessionid import AccessionID
from taxadb.taxid import TaxID

from collections import defaultdict

def getClusterInfo(HMMpath):
    clusterInfo = {}
    with open(HMMpath) as HMMfile:
        for line in HMMfile:
            if line.startswith("LENG"):
                currLength = int(re.search('LENG\s+([0-9]+)',line).group(1))
            if line.startswith("NAME"):
                currID = re.search('NAME.*FAM([0-9]+)',line).group(1)
            if line.startswith("//"):
                clusterInfo = {"id": currID, "length": currLength}
            if line.split() == ("HMM"):
                break
    return clusterInfo


def extractSeqNames(MSApath):
    currMSA = open(MSApath)
    seqNameAnnotations = []
    for line in currMSA:
        if line.startswith(">"):
            seqNameAnnotations.append(line)
    currMSA.close()
    return seqNameAnnotations

def extractHMMannots(tbloutPath):
    lines = (" ".join(l.split()[18:]) for l in open(tbloutPath) if not l.startswith("#"))
    return list(lines)
    

def wordCount(annotList,directName):
    wc = defaultdict(int)
    for currAnnot in annotList:
        field = currAnnot if directName else currAnnot.split("|")[5]
        words = re.findall(r"\w{3,}",field)
        for word in words:
            wc[word.lower()] += 1
    return wc

def getTaxInfo(tdb_accession,tdb_taxid,seqNameAnnotations):
    taxInfo = {"nbSeq":len(seqNameAnnotations)}
    accs = []
    for ca in seqNameAnnotations:
        #accs.append(re.findall("[A-Z0-9\._]{5,10}",ca)[0].split(".")[0])
        accs.append(ca.split("|")[2].split(".")[0])
    taxInfo["taxid"] = []
    currAcc = 0
    # proceeding by lots, to avoid `too many SQL variables` issue
    while currAcc < len(accs):
        currAccs=accs[currAcc:(min(currAcc+249,len(accs)))]
        taxInfo["taxid"].append(tdb_accession.taxid(currAccs))
        currAcc += 249
    # from taxid family
    taxInfo["taxon"] = defaultdict(int)
    
    LCAs = []
    
    # deloting
    for ctgen in taxInfo["taxid"]:
        for ct in ctgen:
            lineage = tdb_taxid.lineage_id(ct[1], reverse=True)
            if len(lineage) == 0:
                continue
            #managing LCA
            if len(LCAs) == 0:
                LCAs = lineage
            else:
                if len(lineage) < len(LCAs):
                    LCAs = lineage
                else:
                    for index,taxon in enumerate(LCAs):
                        if lineage[index] != taxon:
                            LCAs = LCAs[:index]
                            break
            taxon = lineage[-1]
            taxInfo["taxon"][taxon] += 1
    
    taxInfo["LCA"] = (LCAs[-1] if len(LCAs)>0 else 0)
    return taxInfo


def writeAnnotations(dbConn,famID,clusterInfo,taxInfo,seqNameAnnotations,refHMMannotations):
    c = dbConn.cursor()
    c.execute("INSERT INTO family VALUES ({id},{size},{nbSeq},{LCAtaxid});".format(
            id=famID,
            size=clusterInfo["length"],
            nbSeq=taxInfo["nbSeq"],
            LCAtaxid=taxInfo["LCA"]
        ))
    c.executemany("INSERT INTO fam_tax VALUES ({},?,?);".format(famID),taxInfo["taxon"].items())
    
    for (annotations,table,directName) in ((seqNameAnnotations,"fam_kw_seqnames_unsorted",False),(refHMMannotations,"fam_kw_ref_unsorted",True)):
        wc = wordCount(annotations,directName)
        for currWord,currCount in wc.items():
            c.execute("SELECT id FROM keyword WHERE str = ? ;",(currWord,))
            data=c.fetchone()
            if data is None:
                c.execute("INSERT INTO keyword (str) VALUES (?) ;",(currWord,))
                cwid = c.lastrowid
            else:
                cwid = data[0]
            c.execute(f"INSERT INTO {table} VALUES (?,?,?);",(famID,cwid,currCount))

        dbConn.commit()




parser = argparse.ArgumentParser()
parser.add_argument("hmmDir", help="hmm dir")
parser.add_argument("alignedDir", help="directory containing .msa.faa")
parser.add_argument("annotResultsDir", help="directory containing tblout resulting a reference DB query to annotate")
parser.add_argument("taxadb", help="taxadb file")
parser.add_argument("schema", help="sql schema to format the db")
parser.add_argument("annotDB", help="db to create in which annotation will be stored")


args = parser.parse_args()

conn = sqlite3.connect(args.annotDB)
with open(args.schema) as schema:
    conn.executescript(schema.read())

tdb_accession = AccessionID(dbtype='sqlite', dbname=args.taxadb)
tdb_taxid = TaxID(dbtype='sqlite', dbname=args.taxadb)

clusterList = glob.glob(args.hmmDir+"/*.hmm")
for currCluster in clusterList:
    familyName = os.path.basename(currCluster)[:-4]
    clusterInfo = getClusterInfo(f"{args.hmmDir}/{familyName}.hmm")
    if not clusterInfo:
        print(f"Failed to read {familyName}")
        continue
    seqNameAnnotations = extractSeqNames(f"{args.alignedDir}/{familyName}.fasta")
    refHMMannotations = extractHMMannots(f"{args.annotResultsDir}/{familyName}.tblout")
    taxInfo = getTaxInfo(tdb_accession,tdb_taxid,seqNameAnnotations)
    writeAnnotations(conn,clusterInfo["id"],clusterInfo,taxInfo,seqNameAnnotations,refHMMannotations)

# now we need to sort the table fam_kw_unsorted
c = conn.cursor()
c.execute("INSERT INTO fam_kw_seqnames SELECT * FROM fam_kw_seqnames_unsorted ORDER BY freq DESC;")
c.execute("DROP TABLE fam_kw_seqnames_unsorted;")
conn.commit()

c = conn.cursor()
c.execute("INSERT INTO fam_kw_ref SELECT * FROM fam_kw_ref_unsorted ORDER BY freq DESC;")
c.execute("DROP TABLE fam_kw_ref_unsorted;")
conn.commit()

conn.close()
