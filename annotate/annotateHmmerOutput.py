#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import glob,re,argparse,os,sqlite3

from taxadb.accessionid import AccessionID
from taxadb.taxid import TaxID

from collections import defaultdict

def getFamAnnot(famID,annotC,tdb_taxid):
    c = annotC.cursor()
    c.execute("SELECT LCAtaxid FROM family WHERE id=?",(famID,))
    data = c.fetchone()
    lcaTaxid = data[0]
    lineage = tdb_taxid.lineage_name(lcaTaxid)
    if not lineage:
        print("notLineage " + str(famID))
        lineage = ["Unknown"]
    
    c.execute("SELECT str,freq FROM fam_kw JOIN keyword ON fam_kw.kwId = keyword.id WHERE famID=? order by freq desc",(famID,))
    keywords = c.fetchall()
    return(lineage,keywords)



parser = argparse.ArgumentParser()
parser.add_argument("hmmTabular", help="tabular output of a HMMER run")
parser.add_argument("taxaDB", help="taxadb file")
parser.add_argument("annotDB", help="annotation db file")
parser.add_argument("annotatedOutput", help="file to be produced")


args = parser.parse_args()

conn = sqlite3.connect(args.annotDB)

tdb_taxid = TaxID(dbtype='sqlite', dbname=args.taxaDB)

i = open(args.hmmTabular)
o = open(args.annotatedOutput,"w")
for line in i:
    if line.startswith("#"):
        continue
    ls = line.split()
    sname = ls[0]
    qname = ls[2]
    evalue = ls[4]
    famID = int(qname[3:-4])
    lineage,keywords = getFamAnnot(famID,conn,tdb_taxid)
    
    o.write("{sname};{qname};{evalue};{lineage};{keywords}\n".format(
            sname=sname,
            qname=qname,
            evalue=evalue,
            lineage=",".join(lineage),
            keywords=",".join(["{}({})".format(k,w) for k,w in keywords])
        ))
    


conn.close()
